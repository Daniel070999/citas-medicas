<?php

use App\Http\Controllers\Citascontroller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Models\Cliente;
use App\Models\Doctor;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 
Route::get('create', [ClienteController::class, 'create']);

Route::get('listarClientes', [ClienteController::class, 'listar'])->name('listarClientes');

Route::get('listaradmin', [ClienteController::class, 'listaradmin'])->name('listaradmin');
Route::get('/pasienteadmin/{id}', [ClienteController::class, 'pasienteadmin'])->name('pasienteadmin');
Route::post('/pasienteadmin/{persona}', [ClienteController::class, 'pasienteCrea'])->name('pasienteCrea');
Route::post('/pasientead', [ClienteController::class, 'pasienteCre'])->name('pasienteCre');
Route::post('/paciente/{id}', [ClienteController::class, 'pasienteCreate'])->name('pasienteCreate');

Route::get('listarClientes/buscapasiente', [DoctorController::class, 'buscar'])->name('live_search');
Route::get('listarClientes/buscarespe', [DoctorController::class, 'buscarespe'])->name('buscarespe');
Route::get('/buscapasiente', [ClienteController::class, 'buscar'])->name('live');

Route::get('/buscaresp', [ClienteController::class, 'buscaresp'])->name('buscaresp');
Route::post('registrarcliente', [ClienteController::class, 'registrarcliente'])->name('registrarcliente');

Route::post('registrarclien', [ClienteController::class, 'registrarclien'])->name('registrarclien');
Route::get('show/{id}', [ClienteController::class, 'show'])->name('show'); 

Route::get('modificar/{persona}', [ClienteController::class, 'modificar'])->name('modificar'); 

Route::get('modcliente/{external_id}', [ClienteController::class, 'modcliente'])->name('modcliente');

Route::put('mod/{persona}',[ClienteController::class, 'update'])->name('update');
Route::get('/citas/{id}', [Citascontroller::class, 'listar'])->name('citapa');
Route::put('registrarcitanueva', [Citascontroller::class, 'registrarcitanueva'])->name('registrarcitanueva');
Route::put('registrarcitanu', [Citascontroller::class, 'registrarcitanu'])->name('registrarcitanu');
Route::get('/paciente/{id}', [ClienteController::class, 'pasiente'])->name('paciente');
Route::post('/paciente/{id}', [ClienteController::class, 'pasienteCreate'])->name('pasienteCreate');
Route::get('nuevacitaadmin/{id}', [HomeController::class, 'nuevacita'])->name('nuevacitaadmin');
Route::get('/listapacadmin/{id}', [ClienteController::class, 'listapacadmin'])->name('listapacadmin');
/*Doctor */
Route::get('home', DoctorController::class);
 
Route::get('createmedico/{external}', [HomeController::class, 'create'])->name('createmedico');

Route::get('listarDoc', [DoctorController::class, 'listar'])->name('listarClient');
Route::get('rece/{id}/{external}', [DoctorController::class, 'rece'])->name('rece');
Route::post('receguar', [DoctorController::class, 'receguar'])->name('receguar');

Route::post('createmedico', [DoctorController::class, 'store'])->name('createmediconuevo');

Route::get('shows/{id}', [DoctorController::class, 'show'])->name('shows'); 

Route::get('modificars/{persona}', [DoctorController::class, 'modificar'])->name('modificars');

Route::put('mods/{persona}',[DoctorController::class, 'update'])->name('updates');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('guardarhorario',[DoctorController::class, 'guardarhorario'])->name('guardarhorario');

Route::get('agreghorario/{externalID}',[DoctorController::class, 'agreghorario'])->name('agreghorario');
Route::post('cancelarcita/{externalID}',[HomeController::class, 'cancelarcita'])->name('cancelarcita');

Route::get('modhora/{id}',[DoctorController::class, 'modhora'])->name('modhora');
Route::put('modhor/{id}', [DoctorController::class, 'modhor'])->name('modhor');
Route::get('mensaje', [DoctorController::class, 'mensaje'])->name('mensaje');

Route::get('historialexamen/{external}', [DoctorController::class, 'historialexamen'])->name('historialexamen');

Route::get('homecliente',[App\Http\Controllers\HomeController::class,'getCliente'])->name('homecliente');

Route::get('/homemedico',[App\Http\Controllers\HomeController::class,'getMedico'])->name('homemedico');

Route::get('listarcitasadmin', [HomeController::class, 'listarcitasadmin'])->name('listarcitasadmin');

Route::put('modificardatosmedicos/{doctor}', [HomeController::class, 'modificardatosmedicos'])->name('modificardatosmedicos');
Route::put('modificardatoscliente/{persona}', [ClienteController::class, 'modificardatoscliente'])->name('modificardatoscliente');

Route::get('agregarhistor/{persona}/{external}', [DoctorController::class, 'agregarhist'])->name('agregarhist'); 
Route::put('agregarhistorial/{persona}', [DoctorController::class, 'agregarhistorial'])->name('agregarhistorial');
Route::put('agregardiagnostico/{persona}', [DoctorController::class, 'agregardiagnostico'])->name('agregardiagnostico');
Route::get('agregardiag/{persona}', [DoctorController::class, 'agregardiag'])->name('agregardiag');

Route::get('recetas/{id}',[ClienteController::class, 'recetas'])->name('recetas');

Auth::routes();

//pacientes

Auth::routes();


Route::get('salirAdmin', [App\Http\Controllers\HomeController::class, 'salir'])->name('salirAdmin');
Route::get('salirdoc', [App\Http\Controllers\HomeController::class, 'salirdoc'])->name('salirdoc');
Route::get('verhistorialmedicocliente/{cliente}', [DoctorController::class, 'verhistorialmedicocliente'])->name('verhistorialmedicocliente');
Route::get('verhistoriclie/{cliente}', [DoctorController::class, 'verhistoriclie'])->name('verhistoriclie');
Route::get('especialidad/{id}', [DoctorController::class, 'especialidad'])->name('especialidad');

Route::get('verdiagnosticocliente/{cliente}', [DoctorController::class, 'verdiagnosticocliente'])->name('verdiagnosticocliente');
Route::get('verdiagnostico/{cliente}', [ClienteController::class, 'verdiagnostico'])->name('verdiagnostico');
Route::get('nuevacita/{id}', [Citascontroller::class, 'nuevacita'])->name('nuevacita');
Route::get('especialidad', [DoctorController::class, 'especialida'])->name('especialida');
Route::post('especialidad', [DoctorController::class, 'especialidaguar'])->name('especialidagu');
Route::post('agregardoc', [DoctorController::class, 'agreespe'])->name('agreespe');
Route::put('agregar/{persona}', [DoctorController::class, 'agregarresultado'])->name('agregarresultado');
Route::get('pacienteadmin', [HomeController::class, 'pacienteadmin'])->name('pacienteadmin');
Route::get('agrespecilidad/{id}', [DoctorController::class, 'agrespecilidad'])->name('agrespecilidad'); 
Route::get('elimiarespe/{id}', [DoctorController::class, 'elimiarespe'])->name('elimiarespe');

Route::get('elimiarhorario/{id}', [DoctorController::class, 'elimiarhorario'])->name('elimiarhorario');
Route::delete('elimiare', [DoctorController::class, 'elimiare'])->name('elimiare');
Route::delete('elimiarhorar', [DoctorController::class, 'elimiarhorar'])->name('elimiarhorar');
Route::get('descargarPDF/{cliente}/{external}', [ClienteController::class, 'descargarPDF'])->name('descargarPDF');

Route::get('descargarReceta/{cliente}/{external}', [ClienteController::class, 'descargarReceta'])->name('descargarReceta');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('desactivarespecialidad/{external_id}', [HomeController::class, 'desactivarespecialidad'])->name('desactivarespecialidad');
Route::post('desactivarcita/{external_id}', [HomeController::class, 'desactivarcita'])->name('desactivarcita');
Route::post('activarespecialidad/{external_id}', [HomeController::class, 'activarespecialidad'])->name('activarespecialidad');
