$(function () {
 
    $.validator.addMethod("soloLetras", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Solo letras");

    $.validator.addMethod("registro", function (value, element) {
        return this.optional(element) || /^[N]-[0-9]{4}-[R]-[0-9]{3}$/.test(value);
    }, "Ingrese un registro valido");

    $.validator.addMethod("cedula", function (value, element) {
        return this.optional(element) || validarCedula(value);
    }, "Cedula no valida");
    
    $("#formulario").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            cedula: {
                required: true,
                minlength: 10,
                maxlength: 10,
                cedula: true
            },
            apellidos: {
                required: true, 
                soloLetras:true
            },
            nombres: {
                required: true,
                soloLetras: true
            },
            fecha: {
                required: true,
                
            },
            direccion: {
                required: true,
                
            }
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();

        },
        submitHandler: function (form) {
            form.submit();
        }
    });


$("#for_doc").validate({
    errorElement: 'div',
    errorClass: 'help-block', 
    focusInvalid: false,
    ignore: "",
    rules: {
        cedula: {
            required: true,
            minlength: 10,
            maxlength: 10,
            cedula: true
        },
        apellidos: {
            required: true, 
            soloLetras:true
        },
        nombres: {
            required: true,
            soloLetras: true
        },
        fechas: {
            required: true,
        },
        direccion: {
            required: true,
            
        },
        email: {
            required: true,
        
        }, password: {
            required: true,
        }
            
    },
    highlight: function (e) {
        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    },

    success: function (e) {
        $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
        $(e).remove();

    },
    submitHandler: function (form) {
        form.submit();
    }
});
$("#forcita").validate({
    errorElement: 'div',
    errorClass: 'help-block', 
    focusInvalid: false,
    ignore: "",
    rules: {
        hora: {
            required: true,
        }
            
    },
    highlight: function (e) {
        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    },

    success: function (e) {
        $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
        $(e).remove();

    },
    submitHandler: function (form) {
        form.submit();
    }
});

});

function validarCedula(cedula) {
    var cad = cedula.trim();
    var total = 0;
    var longitud = cad.length;
    var longcheck = longitud - 1;

    if (cad !== "" && longitud === 10) {
        for (i = 0; i < longcheck; i++) {
            if (i % 2 === 0) {
                var aux = cad.charAt(i) * 2;
                if (aux > 9)
                    aux -= 9;
                total += aux;
            } else {
                total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
            }
        }

        total = total % 10 ? 10 - total % 10 : 0;

        if (cad.charAt(longitud - 1) == total) {
            return true;
        } else {
            return false;
        }
    }
}










