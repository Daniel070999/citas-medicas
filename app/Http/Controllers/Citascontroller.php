<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cita;
use App\Models\Client;
use App\Models\Cliente;
use App\Models\Doctor;
use App\Models\Doctore;
use App\Models\Doctorespecialidade;
use App\Models\Doctorhorario;
use App\Models\Doctorhour;
use App\Models\Doctorspecialtie;
use App\Models\Especialidade;
use App\Models\Hour;
use App\Models\Persona;
use App\Models\Medicalexame;
use App\Models\Medicalrecomendacione;
use App\Models\Pasiente;
use App\Models\Patient;
use App\Models\People;
use App\Models\Peoples;
use App\Models\prescripcione;
use App\Models\Quote;
use App\Models\Specialtie;
use App\Models\User;
use App\Models\Userclient;
use App\Models\Usercliente;
use Illuminate\Support\Str;
/**
 * @group citas
 *
 * APIs para citas
 */

class Citascontroller extends Controller
{
    /**
       * Para listar todas las citas de todos los pacientes que tiene un cliente
       * @urlParam id string required El external Id del cliente
       * @response scenario=success {
       *  "name": "Citas encontradas",
       *  "roles": ["cliente"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */  
    public function listar($id){
        $user = User::where('external_id','=',$id)->first(); 
        $usercli = Userclient::where('user_id','=',$user->id)->first(); 
        $cliente = Client::where('id','=',$usercli->client_id)->first();
        $pasiente = Patient::where('client_id','=',$cliente->id)->get(); 
        $cita = Quote::all(); 
        $doctor=Doctor::all();
        $persona=Peoples::all();
        $perso = Peoples::all();
        $especialidad = Specialtie::all();
        $verificar = Quote::all()->count();
        if($verificar == 0){
            $vacio = true;
        return view('listarcita ', compact('cita','perso','persona','pasiente','doctor', 'vacio','cliente','especialidad'));

        }else{
            $vacio = false;
        return view('listarcita ', compact('cita','perso','persona','pasiente','doctor', 'vacio','cliente','especialidad'));
        }
    } 

    /**
       * 
       * @response scenario=success {
       *  "name": "Citas encontradas",
       *  "roles": ["cliente"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */  
    public function __invoke(){
        return view('');
    }
    public function create(Request $external){
        $doctor = Doctor::all();
        $persona = Peoples::all(); 
        $pasiente = Patient::all();
        return view('',compact('doctor','pasiente','cita')); 
    }
 
    /**
       *Para registrar una cita 
       * @queryParam FechaCita string required
       * @queryParam state integer required
       * @queryParam tipoCita string required
       * @queryParam  hora string  required
       * @queryParam doctore_id integer required 
       * @queryParam pasiente_id integer required
       * @queryParam external_id string  required
       * @response scenario=success {
       *  "name": "Citas encontradas",
       *  "roles": ["cliente"]
       * }
       * @response status=500 {
       * "message": "User not found"
       *  }
       */  
    public function registrarcitanueva(Request $request){

        $valorr = $request->valor;

        $statefinal = 0;

        if ($valorr == 0) {
            $statefinal = 1;
        } 
        $cita = new Quote();
        $cita->datequotes = $request->fecha;
        $cita->state= $statefinal;
        $cita->quotetype= $request->tipocita;
        $cita->hour=$request->hora;
        $cita->doctor_id=$request->selectlistados;
        $cita->patient_id=$request->paciente;
        $cita->specialtie_id=$request->selectlistauno;
        $cita->external_id =Str::uuid()->toString();
        $cita->save();

        return view('homecliente');
    }

       /**
        * Para registrar una cita como administrador 
       * @queryParam FechaCita string required
       * @queryParam state integer required
       * @queryParam tipoCita string required
       * @queryParam  hora string  required
       * @queryParam doctore_id integer required 
       * @queryParam pasiente_id integer required
       * @queryParam external_id string  required
       * @response scenario=success {
       *  "name": "Citas encontradas",
       *  "roles": ["cliente"]
       * }
       * @response status=500 {
       * "message": "User not found"
       *  }
       */ 
    public function registrarcitanu(Request $request){

        $valorr = $request->valor;

        $statefinal = 0;

        if ($valorr == 0) {
            $statefinal = 1;
        } 
        $cita = new Quote();
        $cita->datequotes = $request->fecha;
        $cita->state= $statefinal;
        $cita->quotetype= $request->tipocita;
        $cita->hour=$request->hora;
        $cita->doctor_id=$request->selectlistados;
        $cita->patient_id=$request->paciente;
        $cita->specialtie_id=$request->selectlistauno;
        $cita->external_id =Str::uuid()->toString();
        $cita->save();

        return view('mensajecita');
    }

       /**
        * Para cargar todas citas que tiene un doctor
       * @urlParam id integer required El external_id de la persona 
       * @response scenario=success {
       *  "name": "Citas encontradas",
       *  "roles": ["cliente"]
       * }
       * @response status=500 {
       * "message": "User not found"
       *  }
       */ 
    public function nuevacita($id){
        $state=1;
        $persona = Peoples::where('external_id',$id)->first(); 
        $pasiente = Patient::where('people_id',$persona->id)->first(); 
        $doctores = Doctor::all(); 
        $citas = Quote::where('state',$state)->get(); 
        $persona = Peoples::all();
        $doctoresespecialidades = Doctorspecialtie::all();
        $especialidades = Specialtie::all();
        
        $citasdos = Quote::where('state',$state)->get(); 
        
        $doctorhorario = Doctorhour::all();
        $horarios = Hour::all();
        return view('nuevacitaadmin', compact('citas','citasdos','doctores','pasiente','persona', 'especialidades', 'doctoresespecialidades', 'doctorhorario', 'horarios'));

        
    }
 
}
