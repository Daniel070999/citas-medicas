<?php

namespace App\Http\Controllers;

use App\Models\Client;

use App\Models\Doctor;
use App\Models\Doctorhour;
use App\Models\Doctorspecialtie;
use App\Models\Hour;
use App\Models\Medicalexam;

use App\Models\Patient;
use App\Models\Peoples;
use App\Models\Prescription;
use App\Models\Quote;
use App\Models\Recipe;
use App\Models\Specialtie;
use App\Models\User;
use App\Models\Userclient;

use App\Models\Userdoctor;
use Barryvdh\DomPDF\Facade as PDF;
use Faker\Provider\ar_JO\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use function PHPUnit\Framework\returnValue;


/**
 * @group Cliente
 *
 * APIs para clientes
 */
class ClienteController extends Controller
{

    /**
     * Para listar el hsitorial medico del cliente 
     * @response scenario=success {
     * "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function listar()
    {

        $clientes = Client::all();
        $personas = Peoples::get();
        $historialmedicos = Medicalexam::all();
        $pasiente = Patient::all();
        $personados = Peoples::all();
        $doctor = Doctor::all();
        $cita = Quote::all();
        $userdoc = Userdoctor::all();
        return view('listarClientes', compact('clientes', 'personas', 'historialmedicos', 'pasiente','personados','doctor','cita','userdoc'));
    }

    public function listaradmin()
    {

        $clientes = Client::all();
        $personas = Peoples::get();
        $verificar = Client::all()->count();
        $tamanio = Peoples::all()->count();
        $correo = User::get('email');
        $tamaniocorreo = User::get('email')->count();
        if ($verificar == 0) {
            $datos = false;
            return view('homeclient', compact('clientes', 'personas', 'datos', 'tamanio', 'correo', 'tamaniocorreo'));
        } else
            $datos = true;
        return view('homeclient', compact('clientes', 'personas', 'datos', 'tamanio', 'correo', 'tamaniocorreo'));
    }
    public function listapacadmin($id)
    {

        $cliente = Client::where('external_id', '=', $id)->first();
        $pasiente = Patient::all();
        $persona = Peoples::all();
        $tamanio = Peoples::all()->count();

        $verificar = Patient::all()->count();

        if ($verificar == 0) {
            $datos = false;
            return view('listarpacadmin', compact('tamanio', 'datos', 'cliente', 'pasiente', 'persona'));
        } else
            $datos = true;
        return view('listarpacadmin', compact('tamanio', 'datos', 'cliente', 'pasiente', 'persona'));
    }

    public function pasienteadmin($id)
    {
        $cliente = Client::where('external_id', '=', $id)->first();

        $cedula = Peoples::get('cedula');
        $correo = User::get('email');
        $tamanio = Peoples::get('cedula')->count();
        $tamaniocorreo = User::get('email')->count();

        return view('pasienteadmin', compact('cliente', 'cedula', 'tamanio', 'correo', 'tamaniocorreo'));
    }

    public function __invoke()
    {
        $cliente = Client::orderBy('id', 'desc')->paginate();


        return view('home');
    }
    /**
     * Para  ingresar a los pacientes 
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function create()
    {
        return view('create');
    }
    /**
     * Para descargar un pdf con los examenes que tiene el cliente
     * @urlParam id strig required El exteranl id de la persona para realizar la consulta
     * @response scenario=success {
     *  "name": "Descarga exitosa",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function descargarPDF($cliente,$external)
    {
        
        $cita = Quote::where('external_id',$external)->first();
        $persona = Peoples::where('external_id', $cliente)->first();
        $pasiente = Patient::Where('people_id', $persona->id)->first();
        $enfermedad = Prescription::where('patient_id',$pasiente->id)->get();
        $mediExa = Medicalexam::where('patient_id', $pasiente->id)
        ->where('quote_id', $cita->id)            
        ->get();
        $datos = true;

        $pdf_doc = PDF::loadView('pdf.descargardiagnostico', compact('datos', 'mediExa', 'pasiente', 'persona','enfermedad'));


        return $pdf_doc->download('Examenes.pdf');
    }
    public function descargarReceta($cliente,$external)
    {
        $cita = Quote::where('external_id',$external)->first();
        $persona = Peoples::where('external_id', $cliente)->first();
        $pasiente = Patient::Where('people_id', $persona->id)->first();
        $enfermedad = Prescription::where('patient_id',$pasiente->id)->get();
        $mediExa = Recipe::where('patient_id', $pasiente->id)
                            ->where('quote_id', $cita->id)
                            ->get();
        $datos = true;

        $pdf_doc = PDF::loadView('pdf.descargareceta', compact('datos', 'mediExa', 'pasiente', 'persona','enfermedad'))->setOptions(['defaultFont' => 'sans-serif']);;


        return $pdf_doc->download('Receta.pdf');
    }
    /**
     * Para registrar aun cliente 
     * @queryParam cedula integer required
     * @queryParam nombres string required
     * @queryParam apellidos string required
     * @queryParam  Ciudadresidencia string  required
     * @queryParam  genero string  required
     * @queryParam  telefono string  required
     * @queryParam  FechaNacimiento string  required
     * @queryParam  email string  required
     * @queryParam  password string  required
     * @queryParam external_id string  required
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     * @response status=500 {
     * "message": "User not found"
     *  }
     */
    public function registrarcliente(Request $request)
    {


        $persona = new Peoples();
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->external_id = Str::uuid()->toString();
        $persona->save();
        $user = new User;
        $user->name = $request->historia;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->acceso = $request->acceso;
        $user->external_id = Str::uuid()->toString();
        $user->save();

        $cliente = new Client;
        $cliente->people_id = $persona->id;
        $cliente->external_id = Str::uuid()->toString();
        $cliente->save();

        $pasiente = new Patient();
        $pasiente->client_id = $cliente->id;
        $pasiente->people_id = $persona->id;
        $pasiente->external_id = Str::uuid()->toString();
        $pasiente->save();

        $usercliente = new Userclient();
        $usercliente->client_id = $cliente->id;
        $usercliente->user_id = $user->id;
        $usercliente->external_id = Str::uuid()->toString();
        $usercliente->save();


        return view('mensajeclient')->with('success', 'creado');
    }
    public function registrarclien(Request $request)
    {


        $persona = new Peoples();
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->external_id = Str::uuid()->toString();
        $persona->save();
        $user = new User;
        $user->name = $request->historia;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->acceso = $request->acceso;
        $user->external_id = Str::uuid()->toString();
        $user->save();

        $cliente = new Client;
        $cliente->people_id = $persona->id;
        $cliente->external_id = Str::uuid()->toString();
        $cliente->save();

        $pasiente = new Patient();
        $pasiente->client_id = $cliente->id;
        $pasiente->people_id = $persona->id;
        $pasiente->external_id = Str::uuid()->toString();
        $pasiente->save();

        $usercliente = new Userclient();
        $usercliente->client_id = $cliente->id;
        $usercliente->user_id = $user->id;
        $usercliente->external_id = Str::uuid()->toString();
        $usercliente->save();


        return view('mensajecliente');
    }

    /**
     * Para obtener el cliente ingresado
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function show($id)
    {

        $persona = Peoples::find($id);

        return view('show', compact('persona'));
    }
    /**
     * Para cargar la interfaz modificar del cliente
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function modificar(Peoples $persona)
    {
        return view('modificares', compact('persona'));
    }

    public function modcliente($external_id)
    {
        $aut = User::where('external_id', $external_id)->first();
        $person = Userclient::where('user_id', $aut->id)->first();
        $cliente = Client::where('id', $person->client_id)->first();
        $persona = Peoples::where('id', $cliente->people_id)->first();
        return view('modificardatoscliente', compact('persona'));
    }

    public function modificardatoscliente(Request $request, Peoples $persona)
    {
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cityResidence = $request->direccion;
        $persona->phone = $request->telefono;
        $persona->save();

        return view('mensaje');
    }
    /**
     * Para actualizar datos del cliente
     * @queryParam cedula integer required
     * @queryParam nombres string required
     * @queryParam apellido string required
     * @queryParam  Ciudadresidencia string  required
     * @queryParam  genero string  required
     * @queryParam  telefono string  required
     * @queryParam  FechaNacimiento string  required
     * @queryParam external_id string  required
     * @response scenario=success {
     * "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     * @response status=500 {
     * "message": "User not found"
     *  }
     */
    public function update(Request $request, Peoples $persona)
    {
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->save();
        return view('show', compact('persona'));
    }

    /**
     * Para obtener a los pacientes que tiene cada cliente
     * @urlParam id string required El external Id del Usuario
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function pasiente($id)
    {
        $auth = User::where('external_id', $id)->first();
        $clienteCuen = Userclient::where('user_id', $auth->id)->first();
        $cliente = Client::where('id', '=', $clienteCuen->client_id)->first();
        $pasiente = Patient::all();
        $persona = Peoples::all();
        $tamanio = Peoples::all()->count();

        $verificar = Patient::all()->count();

        if ($verificar == 0) {
            $datos = false;
            return view('listarpasiente', compact('tamanio', 'datos', 'cliente', 'pasiente', 'persona'));
        } else
            $datos = true;
        return view('listarpasiente', compact('tamanio', 'datos', 'cliente', 'pasiente', 'persona'));
    }
    /**
     * Para registrar cada paciente del cliente
     * @queryParam cedula integer required
     * @queryParam nombres string required
     * @queryParam apellido string required
     * @queryParam  Ciudadresidencia string  required
     * @queryParam  genero string  required
     * @queryParam  telefono string  required
     * @queryParam  FechaNacimiento string  required
     * @queryParam external_id string  required
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     * @response status=500 {
     * "message": "User not found"
     *  }
     */
    public function pasienteCreate(Request $request, $id)
    {
        $cliente = Client::where('external_id', '=', $id)->first();
        $persona = new Peoples();
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->external_id = Str::uuid()->toString();
        $persona->save();
        $pasiente = new Patient();
        $pasiente->client_id = $cliente->id;
        $pasiente->external_id = Str::uuid()->toString();
        $pasiente->people_id = $persona->id;
        $pasiente->save();
        return view('homecliente');
    }
    public function pasienteCrea(Request $request, $persona)
    {

        $cliente = Client::where('external_id', '=', $persona)->first();
        $persona = new Peoples();
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->external_id = Str::uuid()->toString();
        $persona->save();
        $pasiente = new Patient();
        $pasiente->client_id = $cliente->id;
        $pasiente->external_id = Str::uuid()->toString();
        $pasiente->people_id = $persona->id;
        $pasiente->save();
        return view('mesajepacientedos', compact('cliente'));
    }

    public function pasienteCre(Request $request)
    {
        $cliente = Client::where('external_id', '=', $request->cliente)->first();
        $persona = new Peoples();
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->external_id = Str::uuid()->toString();
        $persona->save();
        $pasiente = new Patient();
        $pasiente->client_id = $cliente->id;
        $pasiente->external_id = Str::uuid()->toString();
        $pasiente->people_id = $persona->id;
        $pasiente->save();
        return view('mesajepacidos');
    }
    /**
     * Para obtener a las recetas de los pacientes
     * @urlParam id string required El external Id de la persona
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function recetas($id)
    {
        $persona = Peoples::where('external_id', $id)->first();
        $pasiente = Patient::where('people_id', $persona->id)->first();
        $receta = Recipe::all();
        return view('recetascliente', compact('receta', 'pasiente', 'persona'));
    }

    /**
     * Para obtener todos los examenes que tiene los pacientes
     * @urlParam id string required El external Id del Usuario
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["cliente"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function verdiagnostico($cliente)
    {

        $persona = Peoples::where('external_id', $cliente)->first();
        $pasiente = Patient::Where('people_id', $persona->id)->first();
        $mediExa = Medicalexam::where('patient_id', $pasiente->id)->get();
        $mediE = Medicalexam::where('patient_id', $pasiente->id)->count();
        if ($mediE == 0) {
            $datos = false;

            return view('diagnosticoclie', compact('datos', 'mediExa', 'pasiente', 'cliente'));
        } else {
            $datos = true;
            return view('diagnosticoclie', compact('datos', 'mediExa', 'pasiente', 'cliente'));
        }
    }
    public function buscar(Request $request)
    {
        $query = $request->get('texto');
        $criterio = $request->get('criterio');
        $data = [];
        if ($criterio === 'cedula') {
            $persona = Peoples::where("cedula", "=", $query)->get();
            $doctor = Doctor::all();
            $userdo = Userdoctor::all();
            $user = User::all();
            $especialidad = Specialtie::all();
            $docespecialidad = Doctorspecialtie::all();
            $hou = Hour::all();
            $hourdoc = Doctorhour::all();
            foreach ($persona as $pero) {
                foreach ($doctor as $doc) {
                    foreach ($userdo as $use) {
                        foreach ($user as $u) {
                            foreach ($hou as $ho) {
                                foreach ($hourdoc as $hd) {
                                    if (
                                        $pero->id == $doc->people_id && $u->id == $use->user_id && $doc->id == $use->doctor_id
                                        &&  $hd->doctor_id == $doc->id && $ho->id == $hd->hour_id
                                    ) {
                                        foreach ($especialidad as $key) {
                                            foreach ($docespecialidad as $value) {
                                                if ($key->id == $value->specialtie_id && $doc->id == $value->doctor_id) {
                                                    $data = [
                                                        array(
                                                            'cedula' => $pero->cedula,
                                                            'nombre' => $pero->name,
                                                            'apellidos' => $pero->surname,
                                                            'telefono' => $pero->phone,
                                                            'correo' => $u->email,
                                                            'especialidad' => $key->name,
                                                            'horario' => $ho->entrytime ,
                                                            'doctor' => $doc->external_id,
                                                            'persona' => $pero->external_id
                                                        )
                                                    ];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($data);
        } elseif ($criterio === 'nombres') {
            $persona =  Peoples::where("name", $query)->get();
            $doctor = Doctor::all();
            $userdo = Userdoctor::all();
            $user = User::all();
            $especialidad = Specialtie::all();
            $docespecialidad = Doctorspecialtie::all();
            $hou = Hour::all();
            $hourdoc = Doctorhour::all();
            foreach ($persona as $pero) {
                foreach ($doctor as $doc) {
                    foreach ($userdo as $use) {
                        foreach ($user as $u) {
                            foreach ($hou as $ho) {
                                foreach ($hourdoc as $hd) {
                                    if (
                                        $pero->id == $doc->people_id && $u->id == $use->user_id && $doc->id == $use->doctor_id
                                        &&  $hd->doctor_id == $doc->id && $ho->id == $hd->hour_id
                                    ) {
                                        foreach ($especialidad as $key) {
                                            foreach ($docespecialidad as $value) {
                                                if ($key->id == $value->specialtie_id && $doc->id == $value->doctor_id) {
                                                    $data = [
                                                        array(
                                                            'cedula' => $pero->cedula,
                                                            'nombre' => $pero->name,
                                                            'apellidos' => $pero->surname,
                                                            'telefono' => $pero->phone,
                                                            'correo' => $u->email,
                                                            'especialidad' => $key->name,
                                                            'horario' => $ho->entrytime ,
                                                            'doctor' => $doc->external_id,
                                                            'persona' => $pero->external_id
                                                        )
                                                    ];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($data);
        }
    }

    public function buscaresp(Request $request)
    {
        $query = $request->get('texto');
        $criterio = $request->get('criterio');
        $data = [];
        if ($criterio === 'paciente') {
            $persona = Peoples::where('name', $query)->get();
            $people = Peoples::all();
            $patiente = Patient::all();
            $cita = Quote::all();
            $doctor = Doctor::all();
            $espe = Specialtie::all();
            foreach ($persona as $per) {
                foreach ($patiente as $pat) {
                    foreach ($cita as $ci) {

                        foreach ($doctor as $doc) {

                            foreach ($espe as $esp) {


                                foreach ($people as $peo) {
                                    if (
                                        $ci->patient_id == $pat->id && $per->id == $pat->people_id && $doc->people_id == $peo->id
                                        && $ci->specialtie_id == $esp->id && $ci->doctor_id == $doc->id
                                    ) {
                                        
                                        $data = [
                                            array(
                                                'cedula'=>$per->cedula,
                                                'fecha' => $ci->datequotes,
                                                'hora' => $ci->hour,
                                                'paciente' => $per->name ." ".$per->surname,
                                                'doctor' => $peo->name ." ". $peo->surname,
                                                'especialidad' => $esp->name,
                                                'estado' => $ci->state,
                                                'external' => $ci->external_id

                                            )
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($data);
        } elseif ($criterio === 'doctor') {
            $persona = Peoples::where('name', $query)->get();

            $people = Peoples::all();
            $patiente = Patient::all();
            $cita = Quote::all();
            $cliente = Client::all();
            $doctor = Doctor::all();
            $espe = Specialtie::all();
            foreach ($persona as $per) {
                foreach ($patiente as $pat) {
                    foreach ($cita as $ci) {

                        foreach ($doctor as $doc) {

                            foreach ($espe as $esp) {


                                foreach ($people as $peo) {
                                    if (
                                        $ci->patient_id == $pat->id && $per->id == $doc->people_id && $pat->people_id == $peo->id
                                        && $ci->specialtie_id == $esp->id && $ci->doctor_id == $doc->id
                                    ) {
                                        $data = [
                                            array(
                                                'cedula'=>$per->cedula,
                                                'fecha' => $ci->datequotes,
                                                'hora' => $ci->hour,
                                                'paciente' => $peo->name ." ".$peo->surname,
                                                'doctor' => $per->name ." ". $per->surname,
                                                'especialidad' => $esp->name,
                                                'estado' => $ci->state,
                                                'external' => $ci->external_id

                                            )
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($data);
        } elseif ($criterio === 'hora') {
            $persona = Peoples::all();

            $people = Peoples::all();
            $patiente = Patient::all();
            $cita = Quote::where('hour', $query)->get();

            $doctor = Doctor::all();
            $espe = Specialtie::all();
            foreach ($persona as $per) {
                foreach ($patiente as $pat) {
                    foreach ($cita as $ci) {

                        foreach ($doctor as $doc) {

                            foreach ($espe as $esp) {


                                foreach ($people as $peo) {
                                    if (
                                        $ci->patient_id == $pat->id && $per->id == $pat->people_id && $doc->people_id == $peo->id
                                        && $ci->specialtie_id == $esp->id && $ci->doctor_id == $doc->id
                                    ) {
                                        $data = [
                                            array(
                                                'cedula'=>$per->cedula,
                                                'fecha' => $ci->datequotes,
                                                'hora' => $ci->hour,
                                                'paciente' => $per->name,
                                                'doctor' => $peo->name,
                                                'especialidad' => $esp->name,
                                                'estado' => $ci->state,
                                                'external' => $ci->external_id

                                            )
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($data);
        } elseif ($criterio === 'fecha') {
            $persona = Peoples::all();

            $people = Peoples::all();
            $patiente = Patient::all();
            $cita = Quote::where('datequotes', $query)->get();

            $doctor = Doctor::all();
            $espe = Specialtie::all();
            foreach ($persona as $per) {
                foreach ($patiente as $pat) {
                    foreach ($cita as $ci) {

                        foreach ($doctor as $doc) {

                            foreach ($espe as $esp) {


                                foreach ($people as $peo) {
                                    if (
                                        $ci->patient_id == $pat->id && $per->id == $pat->people_id && $doc->people_id == $peo->id
                                        && $ci->specialtie_id == $esp->id && $ci->doctor_id == $doc->id
                                    ) {
                                        
                                        $data = [
                                            array(
                                                'cedula'=>$per->cedula,
                                                'fecha' => $ci->datequotes,
                                                'hora' => $ci->hour,
                                                'paciente' => $per->name,
                                                'doctor' => $peo->name,
                                                'especialidad' => $esp->name,
                                                'estado' => $ci->state,
                                                'external' => $ci->external_id

                                            )
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($data);
        }
    }
}
