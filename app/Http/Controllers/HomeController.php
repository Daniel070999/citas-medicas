<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cita;
use App\Models\Client;
use App\Models\Cliente;
use App\Models\Doctor;
use App\Models\Doctore;
use App\Models\Doctorespecialidade;
use App\Models\Doctorhorario;
use App\Models\Doctorhour;
use App\Models\Doctorspecialtie;
use App\Models\Especialidade;
use App\Models\Horario;
use App\Models\Hour;
use App\Models\Pasiente;
use App\Models\Patient;
use App\Models\People;
use App\Models\Peoples;
use App\Models\Persona;
use App\Models\Quote;
use App\Models\Specialtie;
use App\Models\Usercliente;
use App\Models\Userdoctor;
use App\Models\Userdoctore;
use Faker\Provider\ar_JO\Person;
use Faker\Provider\UserAgent;
use Illuminate\Http\Request;
use Userdoctores;
/**
 * @group Vistas
 *
 * APIs para manejo de errores, ventanas principales
 */
class HomeController extends Controller{
         
    /**
       * Manejo de vistas para cada usarios
       * @response scenario=success {
       *  "name": "Proceso con exito"
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('soloadmin',['only'=>'index']);
        $this->middleware('solocliente',['only'=>'getCliente']);
        $this->middleware('solomedico',['only'=>'getMedico']);
    }

         
    /**
       * Para listar los pacientes al administrador
       * @response scenario=success {
       *  "name": "Proceso con exito",
       *  "roles": ["administrador"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function pacienteadmin(){ 
        $pasiente= Patient::all();
        $persona = Peoples::all();
        $clientes = Client::all();
        $per= Peoples::all();
        $verificar= Patient::all()->count();
        if($verificar == 0){
            $datos = false;
            return view('listarpasienteadmin',compact('datos','pasiente','persona','per','clientes')); 
        }else
        $datos = true;
        return view('listarpasienteadmin',compact('datos','pasiente','persona','per','clientes')); 
    }
         
    /**
       * Para cerrar sesión
       * @response scenario=success {
       *  "name": "Proceso con exito"
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function salir(){
        return view('salirAdmin');
    }
    public function salirdoc(){
        return view('salirdoc');
    }
      /**
       * Para cargar los datos para ingresar una nueva cita
       * @urlParam id integer required El external id de la persona
       * @response scenario=success {
       *  "name": "Proceso con exito",
       *  "roles": ["administrador"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function nuevacita($id){
        $state=1;
        $persona = Peoples::where('external_id',$id)->first(); 
        $pasiente = Patient::where('people_id',$persona->id)->first(); 
        $doctores = Doctor::all(); 
        $citas = Quote::where('state',$state)->get(); 
        $persona = Peoples::all();
        $doctoresespecialidades = Doctorspecialtie::all();
        $especialidades = Specialtie::all();
        
        $citasdos = Quote::where('state',$state)->get(); 
        
        $doctorhorario = Doctorhour::all();
        $horarios = Hour::all();
        return view('nuevacita', compact('citas','citasdos','doctores','pasiente','persona', 'especialidades', 'doctoresespecialidades', 'doctorhorario', 'horarios'));
    }
    
     /**
       * Para cargar los doctores
       * @urlParam id integer required El external id de la persona
       * @response scenario=success {
       *  "name": "Proceso con exito",
       *  "roles": ["administrador"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function create($external){

        $doctor = Peoples::where('external_id',$external)->first();
        return view('createDoc', compact('doctor'));
    }

     /**
       * Para cargar los datos para ingresar una nueva cita
       * @urlParam doctor required El external id de la persona
       * @queryParam nombres string required
       * @queryParam apellidos string required
       * @queryParam  Ciudadresidencia string  required
       * @queryParam  genero string  required
       * @queryParam  telefono string  required
       * @response scenario=success {
       *  "name": "Proceso con exito",
       *  "roles": ["administrador"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function modificardatosmedicos(Request $request, Peoples $doctor){
       
        $doctor->name = $request->nombres;
        $doctor->surname= $request->apellidos;
        $doctor->cityResidence=$request->direccion;
        $doctor->phone=$request->telefono;
        $doctor->save(); 
        
        return view('mensaje');
        
    }
    
     /**
       * Para redireccionar la ventana principal 
       * @response scenario=success {
       *  "name": "Proceso con exito"
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function index(){

        $userdoctor = Userdoctor::all()->count();
        $userdoctores = Userdoctor::all();
        $doctore = Doctor::all();
        $user = User::all();
        $especialidad = Specialtie::all();
        $doces = Doctorspecialtie::all();
        $persona = Peoples::all();
        $tamanio = Peoples::all()->count();
        $correo = User::get('email');
        $tamaniocorreo = User::get('email')->count();
        $hour = Hour::all();
        $dochour = Doctorhour::all();
        if ($userdoctor == 0) {
            $verificar = true;
            return view('home', compact('correo', 'tamaniocorreo', 'tamanio', 'especialidad', 'userdoctores', 'doctore', 'user', 'persona', 'verificar', 'doces','hour','dochour'));
        } else {
            
                $verificar = false;
                return view('home', compact('correo', 'tamaniocorreo', 'tamanio', 'userdoctores', 'doctore', 'user', 'persona', 'verificar', 'especialidad', 'doces','hour','dochour'));
            
        }
        
    }
         /**
       * Para redireccionar la ventana principal  del cliente
       * @response scenario=success {
       *  "name": "Proceso con exito",
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function getCliente(){
        
        return view('homecliente');
    }
         /**
       * Para redireccionar la ventana principal del doctor 
       * @response scenario=success {
       *  "name": "Proceso con exito",
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function getMedico(){
        $citas = Quote::get();
        $userdoctores = Userdoctor::get();
        $doctores = Doctor::get();
        $pe = Peoples::all();
        $espe = Specialtie::all();
        $pa = Patient::all();
        $cliente =Client::all();
        $personados=Peoples::all();
        //return $citas;
        return view('homemedico', compact('citas','userdoctores','doctores','pe','espe','pa','cliente','personados'));
    }
         /**
       * Para listar todas las citas donde el administrador
       * @response scenario=success {
       *  "name": "Proceso con exito",
       *  "roles": ["administrador"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function listarcitasadmin(){

        $cita = Quote::all();
        $doctore = Doctor::all();
        $paciente = Patient::all();
        $persona = Peoples::all();
        $pe = Peoples::all();
        $especialidad = Specialtie::all();
        $doces = Doctorspecialtie::all();
        $cliente = Client::all();
        $personados = Peoples::all();
        $verificar = Quote::all()->count();
        if($verificar == 0){
            $datos = false;
            return view('listarcitasadmin',compact('datos','cita','doctore','paciente','persona','pe','especialidad','doces','cliente','personados'));
        }else{
            $datos = true;
            return view('listarcitasadmin',compact('datos','cita','doctore','paciente','persona','pe','especialidad','doces','cliente','personados'));
        }
        

        
    }
         /**
       * Para redireccionar al cancelar una cita 
       * @response scenario=success {
       *  "name": "Proceso con exito",
       *  "roles": ["administrador"]
       * }
       *  @response status=500 scenario="user not found" {
       * "message": "User not found"
       *  }
       */
    public function cancelarcita($externalID){
        $cita = Quote::where('external_id',$externalID)->first();
        $cita->state = 0;
        $cita->save();
        return view('mensajecitasdos');

    }
    public function desactivarespecialidad($external_id){
        $especialidad = Specialtie::where('external_id',$external_id)->first();
        $especialidad->active = 0;
        $especialidad->save();
        return view('mensaje');
    }

    public function activarespecialidad($external_id){
        $especialidad = Specialtie::where('external_id',$external_id)->first();
        $especialidad->active = 1;
        $especialidad->save();
        return view('mensaje');
    }
    public function desactivarcita($external_id){
        $especialidad = Quote::where('external_id',$external_id)->first();
        $especialidad->state = 0;
        $especialidad->save();
        return view('mensaje');
    }


}