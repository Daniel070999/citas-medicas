<?php

namespace App\Http\Controllers;

use App\Models\Cita;
use App\Models\Client;
use App\Models\Cliente;
use App\Models\Doctor;
use Illuminate\Http\Request;
use App\Models\Doctore;
use App\Models\Especialidade;
use App\Models\Doctorespecialidade;
use App\Models\Doctorhorario;
use App\Models\Doctorhour;
use App\Models\Doctorspecialtie;
use App\Models\Horario;
use App\Models\Hour;
use App\Models\Medicalexam;
use App\Models\Medicalexame;
use App\Models\Medicalrecomendacione;
use App\Models\Pasiente;
use App\Models\Patient;
use App\Models\People;
use App\Models\Peoples;
use App\Models\Persona;
use App\Models\prescripcione;
use App\Models\Prescription;
use App\Models\Quote;
use App\Models\Receta;
use App\Models\Recipe;
use App\Models\Specialtie;
use App\Models\User;
use App\Models\Userdoctor;
use App\Models\Userdoctore;
use Illuminate\Routing\RedirectController;
use Illuminate\Support\Arr;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\If_;
use Userdoctores;

/**
 * @group Doctores
 *
 * APIs para doctores
 */

class DoctorController extends Controller
{

    /**
     * Para listar los doctores al usuario administrador
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["administrador"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function listar()
    {

        $doctor = Doctor::all();
        $persona = Peoples::paginate();

        return view('listarDoctores', compact('persona'));
    }


    public function __invoke()
    {
        $doctor = Doctor::orderBy('id', 'desc')->paginate();

        return view('homeDoctor');
    }


    /**
     * Mensaje de confirmacion al realizar una accion en el sistema 
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["doctor, administrador"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function mensaje()
    {
        return view('mensaje');
    }
    public function mensajeerror()
    {
        return view('mensajeerror');
    }

    public function historialexamen($external)
    {

        $da = $external;

        return view('resultexamenesdoc', compact('da'));
    }

    /**
     * Para agregar los horarios de los doctores
     * @queryParam horaentrada string required
     * @queryParam horasalida string required
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["administrador"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function guardarhorario(Request $request)
    {

        $externalID =  $request->external;

        $doctor = Doctor::where('external_id', $externalID)->first();
        $horario = new Hour();
        $horario->entrytime = $request->horaentrada;
        $horario->departuretime = $request->horasalida;
        $horario->external_id = Str::uuid()->toString();
        $horario->save();
        $doctorhorario = new Doctorhour();
        $doctorhorario->doctor_id = $doctor->id;
        $doctorhorario->hour_id = $horario->id;
        $doctorhorario->external_id = Str::uuid()->toString();
        $doctorhorario->save();



        return redirect('mensaje');
    }
    /**
     * Para agregar el historial medico  al paciente
     * @urlParam id integer required El external id de la persona en cita
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function agregarhist($persona,$external)
    {
        $persona = Peoples::where('external_id', $persona)->first();
        $paciente = Patient::where('people_id', '=', $persona->id)->first();
        $cita = Quote::where('external_id',$external)->first();
        return view('historial', compact('persona', 'paciente','cita'));
    }
    /**
     * Para agregar una cita para realizar un examen  al paciente
     * @urlParam id integer required El external id de la persona en cita
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function agregardiag($persona)
    {
        $persona = Peoples::where('external_id', $persona)->first();
        $paciente = Patient::where('people_id', '=', $persona->id)->first();
        return view('citaexamen', compact('persona', 'paciente'));
    }
    public function agreghorario($external)
    {
        $doctor = Doctor::where('external_id', $external)->first();
        return view('horarios', compact('doctor'));
    }
    /**
     * Para ingresar datos el historial al paciente
     * @urlParam id integer required El external id de la persona en cita
     * @queryParam Enfermedades string required
     * @queryParam enfermedadesHerdi string required
     * @queryParam Habitos string required
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function agregarhistorial(Request $request, $persona)
    {
        $vroles = "";
        if ($request->input('selectlistauno') != null) {
            $vroles = implode(',', $request->input('selectlistauno'));
        }
        
        $persona = Peoples::where('external_id', $persona)->first();
        $paciente = Patient::where('people_id', '=', $persona->id)->first();
        $cita = Quote::where('external_id',$request->cita)->first();

        $historial = new Prescription();
        $historial->Diseases = $request->enf;
        $historial->diseasesHere = $request->enf_her;
        $historial->habits = $request->hab;
        $historial->patient_id = $paciente->id;
        $historial->external_id = Str::uuid()->toString();
        $historial->save();

        $examenes = new Medicalexam();
        $examenes->description = $request->descripcion;
        $examenes->typeExa = $request->tipoExa;
        $examenes->examdate = $request->fechaex;
        $examenes->hour = $request->hora;
        $examenes->diagnosis = $request->enf;
        $examenes->outcome = "";
        $examenes->quote_id = $cita->id;
        $examenes->patient_id = $paciente->id;
        $examenes->external_id = Str::uuid()->toString();
        $examenes->save();

        $receta = new Recipe();
        $receta->amount = $request->cantidad;
        $receta->product = $vroles;
        $receta->description = $request->descripcion;
        $receta->patient_id = $paciente->id;
        $receta->quote_id = $cita->id;
        $receta->external_id = Str::uuid()->toString();
        $receta->save();
        return view('mensaje');
    }
    /**
     * Para ingresar un examen al  paciente
     * @urlParam id integer required El external id de la persona en cita
     * @queryParam descripcion string required
     * @queryParam tipoExa string required
     * @queryParam fechaexamen string required
     * @queryParam hora string required
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function agregardiagnostico(Request $request, $persona)
    {


        $perso = Peoples::where('external_id', $persona)->first();
        $paciente = Patient::where('people_id', $perso->id)->first();
        $examenes = new Medicalexam();
        $examenes->description = $request->descripcion;
        $examenes->typeExa = $request->tipoExa;
        $examenes->examdate = $request->fechaex;
        $examenes->hour = $request->hora;
        $examenes->diagnosis = "";
        $examenes->outcome = "";
        $examenes->patient_id = $paciente->id;
        $examenes->external_id = Str::uuid()->toString();
        $examenes->save();

        return redirect('listarClientes');
    }

    /**
     * Para ingresar un examen al  paciente
     * @urlParam id integer required El external id de la persona en cita
     * @queryParam cedula integer required
     * @queryParam nombres string required
     * @queryParam apellidos string required
     * @queryParam  Ciudadresidencia string  required
     * @queryParam  genero string  required
     * @queryParam  telefono string  required
     * @queryParam  FechaNacimiento string  required
     * @queryParam  email string  required
     * @queryParam  password string  required
     * @queryParam external_id string  required
     * @response scenario=success {
     *  "name": "Proceso con exito",
     *  "roles": ["administrador"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function store(Request $request)
    {
        $persona = new Peoples();
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fechas;
        $persona->external_id = Str::uuid()->toString();
        $persona->save();
        $doctor = new Doctor;
        $doctor->people_id = $persona->id;
        $doctor->external_id = Str::uuid()->toString();
        $doctor->save();
        $user = new User;
        $user->name = $request->usuario;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->acceso = $request->acceso;
        $user->external_id = Str::uuid()->toString();
        $user->save();
        $userdoctor = new Userdoctor();
        $userdoctor->doctor_id = $doctor->id;
        $userdoctor->user_id = $user->id;
        $userdoctor->external_id = Str::uuid()->toString();
        $userdoctor->save();
        $especialidad = Specialtie::where('external_id', $request->especialidad)->first();
        $dcotorespecialidad = new Doctorspecialtie();
        $dcotorespecialidad->doctor_id = $doctor->id;
        $dcotorespecialidad->specialtie_id = $especialidad->id;
        $dcotorespecialidad->external_id = Str::uuid()->toString();
        $dcotorespecialidad->save();

        return redirect('/')->with('success', 'creado');
    }


    /**
     * Para obtener el doctor ingresado
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function show($id)
    {

        $persona = Peoples::find($id);

        return view('show', compact('persona'));
    }

    /**
     * Para obtener el doctor a modificar
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function modificar(Doctor $doctore)
    {
        $persona = Peoples::where('id', '=', $doctore->people_id);
        return view('modificar', compact('persona'));
    }

    /**
     * Para actulizar datos  del doctor 
     *@urlParam persona string required Enviamos los datos del doctor actulizados
     * @queryParam nombres string required
     * @queryParam apellidos string required
     * @queryParam  Ciudadresidencia string  required
     * @queryParam  genero string  required
     * @queryParam  telefono string  required
     * @queryParam  FechaNacimiento string  required
     * @queryParam external_id string  required 
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function update(Request $request, Peoples $persona)
    {
        $persona->name = $request->nombres;
        $persona->surname = $request->apellidos;
        $persona->cedula = $request->cedula;
        $persona->cityResidence = $request->direccion;
        $persona->gender = $request->genero;
        $persona->phone = $request->telefono;
        $persona->birthdate = $request->fecha;
        $persona->save();

        return view('show', compact('persona'));
    }
    /**
     * Para obtener el historial del paciente
     *@urlParam id string required El external_id de la persona
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function verhistorialmedicocliente($cliente)
    {
        $client = Peoples::where('external_id', $cliente)->first();
        $paciente = Patient::where('people_id', $client->id)->first();
        $cliente = Client::where('id',$paciente->client_id)->first();
        $persona = Peoples::where('id',$cliente->people_id)->first();
        $historialmedico = Prescription::where('patient_id', $paciente->id)->get();
        $mediExa = Medicalexam::where('patient_id', $paciente->id)->get();
        $cita = Quote::all();
        $mediE = Medicalexam::where('patient_id', $paciente->id)->count();
        if ($mediE == 0) {
            $datos = false;
            $da = "";
            return view('historialmedicocliente', compact('historialmedico', 'paciente', 'client', 'da', 'datos', 'mediExa','cita','persona'));
        } else {
            $datos = true;
            $da = "";
            return view('historialmedicocliente', compact('historialmedico', 'paciente', 'client', 'da', 'datos', 'mediExa','cita','persona'));
        }
    }
    /**
     * Para obtener todos los examenes del paciente
     *@urlParam id string required El external_id de la persona
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function verdiagnosticocliente($cliente)
    {


        $persona = Peoples::where('external_id', $cliente)->first();
        $pasiente = Patient::Where('people_id', $persona->id)->first();
        $mediExa = Medicalexam::where('patient_id', $pasiente->id)->get();
        $mediE = Medicalexam::where('patient_id', $pasiente->id)->count();
        if ($mediE == 0) {
            $datos = false;
            $da = "";
            return view('diagnosticocliente', compact('da', 'datos', 'mediExa', 'pasiente'));
        } else {
            $datos = true;
            $da = "";
            return view('diagnosticocliente', compact('da', 'datos', 'mediExa', 'pasiente'));
        }
    }

    /**
     * Para cargar todas las especialidades, para ingresar a los doctores
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function especialida()
    {
        $especialida = Specialtie::all();
        $verificar = Specialtie::all()->count();

        if ($verificar == 0) {
            $datos = false;
            return view('listarespe', compact('datos', 'especialida'));
        } else {
            $datos = true;
            return view('listarespe', compact('datos', 'especialida'));
        }
    }
    /**
     * Para agregar una receta al paciente
     *@urlParam id string required El external_id de la persona
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function rece($id,$external)
    {
        $persona = Peoples::where('external_id', $id)->first();
        $pasiente = Patient::where('people_id', '=', $persona->id)->first();
        $cita = Quote::where('external_id',$external)->first();
        return view('receta', compact('pasiente','cita'));
    }
    /**
     * Para ingresar datos de la receta al paciente
     * @queryParam cantidad string required
     * @queryParam prodcuto string required
     *  @queryParam descripcion string required
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function receguar(Request $request)
    {
        $vroles = "";
        if ($request->input('selectlistauno') != null) {
            $vroles = implode(',', $request->input('selectlistauno'));
        }
        $receta = new Recipe();
        $receta->amount = $request->cantidad;
        $receta->product = $vroles;
        $receta->description = $request->descripcion;
        $receta->patient_id = $request->paciente;
        $receta->quote_id = $request->cita;
        $receta->external_id = Str::uuid()->toString();
        $receta->save();
        return view('mensaje');
    }
    /**
     * Para agregar los resultados de los examenes realizados al paciente
     *@urlParam id string required El external_id de la persona
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */
    public function agregarresultado(Request $request,  $persona)
    {
        $examenes = Medicalexam::where('external_id', $persona)->first();
        $examenes->diagnosis = $request->enf;
        $examenes->outcome = $request->enf_her;
        $examenes->save();

        return view('mensajemedico');
    }
    /**
     * Para agregar las especialidades
     * @queryParam nombre string required
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */

    public function especialidaguar(Request $request)
    {
        $especialida = Specialtie::all();
        foreach ($especialida as $espe) {
            if ($request->nombres == $espe->name) {
                return view('mensajerror');
            }
        }

        $especialidad = new Specialtie();

        $especialidad->name = $request->nombres;
        $especialidad->active = 1;
        $especialidad->external_id = Str::uuid()->toString();
        $especialidad->save();
        return view('mensajeesp');
    }
    /**
     * Para agregar las especialidades que tiene un doctor
     * @urlParam id string required El external_id del doctore
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */


    public function agreespe(Request $request)
    {
        $traerdoc = Doctor::where('id', $request->doctor)->first();
        $traerespe = Doctorspecialtie::all();

        foreach ($traerespe as $persona) {
            if ($persona->specialtie_id == $request->especialidad && $traerdoc->id == $persona->doctor_id) {
                return view('mensajeerror');
            }
        }


        $docEspe = new Doctorspecialtie();
        $docEspe->doctor_id = $request->doctor;
        $docEspe->specialtie_id = $request->especialidad;
        $docEspe->external_id = Str::uuid()->toString();
        $docEspe->save();
        return view('mensaje');
    }

    public function agrespecilidad($id)
    {
        $doctor = Doctor::where('external_id', $id)->first();
        $especialida = Specialtie::all();
        return view('agregespe', compact('doctor', 'especialida'));
    }
    public function elimiarespe($id)
    {
        $doctor = Doctor::where('external_id', $id)->first();
        $especialida = Specialtie::all();
        $docespe = Doctorspecialtie::where('doctor_id', $doctor->id)->get();
        return view('elimiespe', compact('doctor', 'especialida', 'docespe'));
    }

    public function elimiare(Request $request)
    {

        $docest = Doctorspecialtie::where('doctor_id', $request->doctor)
            ->where('specialtie_id', $request->especialidad)
            ->first();
        $docest->delete();
        return view('mensaje');
    }

    public function especialidad($id)
    {
        $doctor = Doctor::where('external_id', $id)->first();
        $perso = Peoples::where('id', $doctor->people_id)->first();
        $especialida = Specialtie::all();
        $docespe = Doctorspecialtie::where('doctor_id', $doctor->id)->get();
        return view('verespe', compact('doctor', 'especialida', 'docespe', 'perso'));
    }
    public function modhora($id)
    {
        $doctor = Doctor::where('external_id', $id)->first();
        $doctohour = Doctorhour::where('doctor_id', $doctor->id)->get();
        $hour = Hour::all();
        return view('modhoraria', compact('doctor', 'doctohour', 'hour'));
    }
    public function elimiarhorario($id)
    {
        $doctor = Doctor::where('external_id', $id)->first();
        $doctohour = Doctorhour::where('doctor_id', $doctor->id)->get();
        $hour = Hour::all();
        return view('eliminarhorario', compact('doctor', 'doctohour', 'hour'));
    }
    public function elimiarhorar(Request $request)
    {
        $doctor = Doctor::where('external_id', $request->doctor)->first();
        $hour = Hour::where('id', $request->hora)->first();
        $doctohour = Doctorhour::where('hour_id', $request->hora)
            ->where('doctor_id', $doctor->id)
            ->first();
        $doctohour->delete();
        $hour->delete();
        return view('mensaje');
    }
    public function modhor(Request $request, $id)
    {

        $hour = Hour::where('external_id', $request->horario)->first();
        $hour->entrytime = $request->horaentrada;
        $hour->departuretime = $request->horasalida;
        $hour->save();
        return view('mensaje');
    }
    /**
     * Micro servicio de busqueda de los pacientes
     * @bodyParam texto string required
     * @response scenario=success {
     *  "name": "Proceso exitoso",
     *  "roles": ["doctor"]
     * }
     *  @response status=500 scenario="user not found" {
     * "message": "User not found"
     *  }
     */

    public function buscar(Request $request)
    {
        $query = $request->get('texto');
        $persona = Peoples::where("cedula", "=", $query)->first();
        $cliente = Client::where('people_id', $persona->id)->first();
        $data = [];
        if (!empty($cliente)) {
            $data = [];
            $data = [
                array(
                    'name' => $persona->name,
                    'surname' => $persona->surname,
                    'cedula' => $persona->cedula,
                    'gender' => $persona->gender,
                    'phone' => $persona->phone,
                    'external_id' => $persona->external_id,
                    'cliente' => $cliente->external_id,
                )
            ];
        } else {
            $data = [
                array(
                    'name' => $persona->name,
                    'surname' => $persona->surname,
                    'cedula' => $persona->cedula,
                    'gender' => $persona->gender,
                    'phone' => $persona->phone,
                    'external_id' => $persona->external_id,
                )
            ];
        }

        echo json_encode($data);
    }
    public function buscarespe(Request $request)
    {
        $query = $request->get('texto');
        $persona = Specialtie::where("name", "=", $query)->get();

        echo json_encode($persona);
    }

    public function verhistoriclie($cliente)
    {
        $cliente = Peoples::where('external_id', $cliente)->first();
        $paciente = Patient::where('people_id', $cliente->id)->first();
        $historialmedico = Prescription::where('patient_id', $paciente->id)->get();
        $mediExa = Medicalexam::where('patient_id', $paciente->id)->get();
        $mediE = Medicalexam::where('patient_id', $paciente->id)->count();
        $cita = Quote::all();
        if ($mediE == 0) {
            $datos = false;
            $da = "";
            return view('verhistoriclie', compact('historialmedico', 'paciente', 'cliente', 'da', 'datos', 'mediExa','cita'));
        } else {
            $datos = true;
            $da = "";
            return view('verhistoriclie', compact('historialmedico', 'paciente', 'cliente', 'da', 'datos', 'mediExa','cita'));
        }
    }
}
