<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SoloAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->acceso=='admin'):
            return $next($request);
        elseif(Auth::user()->acceso=='medico'):
            return redirect('homemedico'); 
        elseif(Auth::user()->acceso=='cliente'):
            return redirect('homecliente');
        endif;

    }
}
