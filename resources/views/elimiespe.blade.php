@extends('layouts.app')
@section('navegadorapp')
@endsection
@section('navegador')
<!-- Sidebar -->
<!-- Fin sidebar -->


   <!-- Fin Navbar -->
@endsection
@section('scripts')
<script src="{{asset('assets/metodos.js')}}"></script>

@endsection

@section('cuerpo')
<div class="container" style="margin-block: 1%">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Registro de receta</div>

                <div class="card-body">
                    <form  id="formulario" method="POST" action="{{route('elimiare')}}"> 
                        @csrf
                        @method('delete')
                        <div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                  <div class="form-group">
                                    <label for="Fecha">Seleccione su especialidad:</label> 
                                      <select name="especialidad" id="especialidad" class="form-control" aria-describedby="espeHelp">
                                        @foreach ($especialida as $item)
                                        @foreach ($docespe as $ite)
                                        @if ($doctor->id == $ite->doctor_id && $item->id ==$ite->specialtie_id)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                        
                                        @endforeach  
                                        
                                          
                                      </select>            
                                  </div>
                                  <input type="text" value="{{$doctor->id}}" hidden id="doctor" name="doctor">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">                                    
                          <a href="{{asset('/')}}" type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</a>
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </div>
                            
                    </form>
                
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection