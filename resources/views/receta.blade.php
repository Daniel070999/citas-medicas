@extends('layouts.app')
@section('navegadorapp')
@endsection
@section('navegador')
<!-- Sidebar -->
<!-- Fin sidebar -->


   <!-- Fin Navbar -->
@endsection
@section('scripts')
<script src="{{asset('assets/metodos.js')}}"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
@endsection

@section('cuerpo')
<div class="container" style="margin-block: 1%">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Registro de receta</div>

                <div class="card-body">
                    <form  id="formulario" method="POST" action="{{route('receguar')}}"> 
    
                        @csrf
                        <div style="padding-left:150px; padding-right: 150px">
                            <ul class="nav nav-tabs">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">RECETAS</a></li>
                               
                            </ul>
                            <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <label class="text-black">Seleccione la especialidad</label>
                                 <select class="selectpicker form-control" multiple id="selectlistauno" name="selectlistauno[]">
                                    <option value="Aspirina">Aspirina</option>
                                    <option value="Omeprazol">Omeprazol</option>
                                    <option value="Lexotiroxina sodica">Lexotiroxina sódica</option>
                                    <option value="Ramipril">Ramipril</option>
                                    <option value="Paracetamol">Paracetamol </option>
                                    <option value="Atorvastatina ">Atorvastatina </option>
                                    <option value="Salbutamol">Salbutamol </option>
                                     </select>
                                <div class="form-group">
                                    <label for="cantidad">Cantidad:</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control " id="cantidad" name="cantidad" placeholder="Ingrese cantidad">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="descripcion" class="control-label">Descripción:</label>
                                    <div class="modal-body">
                                      <textarea class="form-control col-xs-12" id="descripcion" name="descripcion"></textarea>
                                  </div>
                                </div>
                             <input type="text" value="{{$cita->id}} "  hidden id="cita" name="cita">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-outline-primary">Generar receta</button>
                                <a href="{{asset('home') }}" class="btn btn-outline-secondary">Cancelar</a>
                            </div>
                            <div class="form-group">
                                
                                <div class="input-group" hidden>
                                    <input  hidden type="number" class="form-control " id="paciente" name="paciente" value="{{$pasiente->id}}">
                                </div>
                            </div>
                            
                    </form>
                
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
