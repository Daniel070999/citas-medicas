 
@extends('layouts.app')

@section('navegadorapp')
@endsection 

@section('navegador') 
<div class="col-md-3 col-sm-3 col-xs-12">
    <div class="logo">
        <h2><a href="#"></a></h2>
    </div>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="menu">
        <ul class="nav navbar-nav">
            <li class="active"><a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
            <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
            <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
            <li><a href="{{route('listaradmin')}}">Clientes</a></li>
            <li> <a  href="{{route('especialida')}}">Especialidad</a>
          </li>
            <li><a  href="{{route('salirAdmin')}}">Cerrar sesión</a></li>
        </ul>
    </div>
  </div>
    <!-- Start header -->
    
  
  <!-- Header End -->
@endsection
@section('cuerpo')
  <!--modal-->
  <div class="modal fade" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Registro de Pacientes</h4>
            </div>
            <div class="modal-body">
                <form id="formulario" method="POST" action="">
                    @csrf
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos cita</a></li>
                        </ul>

                        <!-- Tab panes --> 
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="form-group">
                                    <label for="cedula">Cedula</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" id="cedula" name="cedula" placeholder="Ingrese su cedula">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nombres">Nombres:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese sus nombres">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="apellidos">Apellidos:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Ingrese sus apellidos">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="direccion">Direccion:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese su direccion">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Telefono:</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control " id="telefono" name="telefono" placeholder="Ingrese su telefono">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Genero:</label>
                                    <select name="genero" id="genero" class="form-control" aria-describedby="espeHelp">
                                        <option value="masculino">Masculino</option>
                                        <option value="femenino">Femenino</option>
                                    </select>            
                                </div>
                                <div class="form-group">
                                    <label for="fecha">Fecha de nacimiento:</label>
                                    <div class="input-group">
                                        <input type="Date" class="form-control " id="fecha" name="fecha" >
                                    </div>
                                </div>

                            </div>
                            

                        </div>
                    </div>
                    <div class="modal-footer">                                    
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin modal -->


<div class="container" style="padding-block: 5%">
  <h2>Lista de Pacientes
  </h2>
  <div class="panel">

    @if ($datos == false)
    <p class="h3 text-center">No hay pacientes registrados</p>
    @else
        
   


    <div class="panel-body">
       
  <div class="row">
    <div class="col">
      <table class="table table-striped table-bordered table-hover" id="tablas">
        <thead>
          <tr>
            <th>Cedula</th>
            <th>Nombre</th>
            <th>Apellido</th>
         
            <th>Cliente</th>
            <th>Género</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody> 
            
            @foreach ($pasiente as $item)
            @foreach ($persona as $ite)
            @foreach ($clientes as $clie)
            @foreach ($per as $pe)
                 
             
            
            @if ( $item->people_id ==$ite->id && $clie->id == $item->client_id && $pe->id == $clie->people_id)
            <tr>
                <td>{{$ite->cedula}}</td>
                <td>{{$ite->name}}</td>
                <td>{{$ite->surname}}</td>
                <td>{{$pe->name}} {{$pe->surname}}</td>
                <td>{{$ite->gender}}</td> 
             
                <td><a href="{{route('nuevacita',$ite->external_id)}}" type="button" class="btn btn-outline-secondary" data-dismiss="modal">Crear cita</a></td>
              </tr> 
            @endif
            @endforeach
            @endforeach  
            @endforeach                 
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @endif
  
@endsection
@section('scripts')
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection