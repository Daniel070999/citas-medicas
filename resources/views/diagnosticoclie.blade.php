

@extends('layouts.app')

@section('navegadorapp')
    
@endsection
@section('navegador')


  @section('cuerpo')
  @if ($datos == false)
            <a type="text" style="font-size: 35px; margin-left: 15%">No hay exmenes</a>
  @else
  
  <input hidden type="text" value="{{$cliente}}" id="externalcliente" name="externalcliente">
    <a href="{{asset('/')}}" style=" margin-left:  90%;margin-block: 1%;" type="button" class="btn btn-outline-warning" >Cancelar</a>
    <div class="container" style="padding-block: 5%">
      <h2>Diagnosticos
      </h2> 
      <div class="row">
        <div class="col"> 
           
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>hora</th>
                <th>fecha</th>
                <th>tipo de examen</th>
                <th>Diagnostico</th>
                <th>Resultado</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody> 
                    @foreach ($mediExa as $diagnostic)
                    <tr>
                      <td>{{$diagnostic->hour}}</td>
                      <td>{{$diagnostic->examdate}}</td>
                      <td>{{$diagnostic->typeExa}}</td>
                      <td>{{$diagnostic->diagnosis}}</td>
                      <td>{{$diagnostic->outcome}}</td>
                      <td>
                        <a href="{{route('descargarPDF',$cliente)}}" class="btn btn-primary pull-right" >Descargar Pdf</a>
                        </td>
                       
                    </tr>
                    @endforeach
            </tbody>
          </table>
      </div>   
      
     

      
    @endif
  @endsection
@endsection


