@extends('layouts.app')
@section('navegadorapp')
@endsection
@section('navegador')
<!-- Sidebar -->
<!-- Fin sidebar -->


   <!-- Fin Navbar -->
@endsection
@section('scripts')
<script src="{{asset('assets/metodos.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$(document).ready(function () {
    
    $('#selectlistauno').change(function(){
        recargarlista();
    });
    $('#selectlistauno').change(function(){
        cargarhorarios();
    });
    $('#selectlistados').change(function(){
        cargarhorarios();
    }); 
    recargarlista();
    cargarhorarios();
    $("#horass");
    $("#fechass");
    var a = recorrerSelect(document.getElementById("horass"));
    var b = recorrerSelect(document.getElementById("horario"));
    console.log(a);
    console.log(b);
    var c = recorrerSelect(document.getElementById("fechass"));
    console.log(c);
    fechaActual();
    selectColor();
    fechasPasadas();
    nuevaFecha(); 
    function nuevaFecha() {
	if (!obtenerHora()) {
		console.log("logica");
		if (fechaActual() == fechaActual()) {
			fechaSiguiente($("#fecha").val());
		}
	}
    $("#fecha").change(function () {
	var f = $("#fecha").val();
	console.log("fecha seleecionada:" + f);
	contieneValor(c, f);  
    var verFecha = $("#fecha").val();
	var horas = $("#horario").val();
	contieneValor(c, verFecha);
	if (contieneValor(c, verFecha) == verFecha && contieneValor(a, horas) == horas) {
		$("select option[value='" + horas + "']").prop("hidden", true);
	};
    var datonuevo = '';
    var valor =[];
    valor[1]= '8:00-8:30';
    valor[2]= '8:30-9:00';
    valor[3]= '9:00-9:30';
    valor[4]= '9:30-10:00';
    valor[5]= '10:00-10:30';
    valor[6]= '10:30-11:00';
    valor[7]= '11:00-11:30';
    valor[8]= '11:30-12:00';
    valor[9]= '12:00-12:30';
    valor[10]= 'almuerzo';
    valor[11]= '16:00-16:30';
    valor[12]= '16:30-17:00';
    valor[13]= '17:00-17:30';
    valor[14]= '17:30-18:00';
    valor[15]= '18:00-18:30';
    for (let index = 1; index < 16; index++) {
        if (contieneValor(c, verFecha) == verFecha && contieneValor(a, valor[index]) == valor[index]) {
            
	    }else{
             datonuevo+= '<option value="'+valor[index]+'" id="h'+index+'">'+valor[index]+'</option>\n';
        };
    }
    $('#horario').html(
        datonuevo
    )
    }); 
}


function llenarhoracomparar() {
	var horas = $("#horario").val();
    if (horas == "8:00-8:30"){
        $("#horacomparar").val("8:00");
    }else if(horas == "8:30-9:00"){
        $("#horacomparar").val("8:30");
    }else if(horas == "9:00-9:30"){
        $("#horacomparar").val("9:00");
    }else if(horas == "9:30-10:00"){
        $("#horacomparar").val("9:30");
    }else if(horas == "10:00-10:30"){
        $("#horacomparar").val("10:00");
    }else if(horas == "10:30-11:00"){
        $("#horacomparar").val("10:30");
    }else if(horas == "11:00-11:30"){
        $("#horacomparar").val("11:00");
    }else if(horas == "11:30-12:00"){
        $("#horacomparar").val("11:30");
    }else if(horas == "12:00-12:30"){
        $("#horacomparar").val("12:00");
    }else if(horas == "16:00-16:30"){
        $("#horacomparar").val("16:00");
    }else if(horas == "16:30-17:00"){
        $("#horacomparar").val("16:30");
    }else if(horas == "16:30-17:00"){
        $("#horacomparar").val("17:00");
    }else if(horas == "17:00-17:30"){
        $("#horacomparar").val("17:00");
    }else if(horas == "17:30-18:00"){
        $("#horacomparar").val("17:30");
    }else if(horas == "18:00-18:30"){
        $("#horacomparar").val("18:00");
    }
}
function cargarhorarios(){
    var iddoctor = $('#selectlistados').val();
    if(iddoctor ==  1){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 1 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  2){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 2 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  3){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 3 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  4){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 4 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  5){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 5 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  6){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 6 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  7){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 7 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  8){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 8 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  9){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 9 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  10){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 10 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  11){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 11 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  12){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 12 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  13){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 13 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  14){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 14 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  15){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 15 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  16){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 16 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  17){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 17 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  18){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 18 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  19){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 19 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(iddoctor ==  20){
        $('#horadoc').html(
            '@foreach($doctores as $doctor)'+
            '@foreach($doctorhorario as $horariodoct)'+
            '@foreach($horarios as $horario)'+
            '@if ($doctor->id == 20 && $doctor->id == $horariodoct->doctor_id && $horario->id == $horariodoct->hour_id)'+
            '<option value="{{$horario->entrytime}}-{{$horario->departuretime}}">Entrada: {{$horario->entrytime}} || Salida: {{$horario->departuretime}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }
}

function recargarlista(){
    var idespecialidad = $('#selectlistauno').val();
    
    if(idespecialidad == 1){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 1)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 2){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 2)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if (idespecialidad == 3){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 3)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 4){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 4)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 5){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 5)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 6){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 6)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 7){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 7)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 8){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 8)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 9){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 9)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 10){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 10)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 11){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 11)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 12){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 12)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 13){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 13)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 14){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 14)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 15){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 15)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 16){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 16)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 17){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 17)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 18){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 18)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 19){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 19)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }else if(idespecialidad == 20){
        $('#selectlistados').html(
            '@foreach($doctoresespecialidades as $doctoresespecialidade)'+
            '@foreach($doctores as $item)'+
            '@foreach($persona as $ite)'+
            '@if($item->people_id ==$ite->id && $item->id == $doctoresespecialidade->doctor_id && $doctoresespecialidade->specialtie_id == 20)'+
            '<option value="{{$item->id}}">{{$ite->name}} {{$ite->surname}}</option>'+
            '@endif'+
            '@endforeach'+
            '@endforeach'+
            '@endforeach'
            );
    }
    


}
});
$("#horario").change(function () {
	var verFecha = $("#fecha").val();
	var horas = $("#horario").val();
	contieneValor(c, verFecha);
	    if (contieneValor(c, verFecha) == verFecha && contieneValor(a, horas) == horas) {
		    $("select option[value='" + horas + "']").prop("hidden", true);
	    }
    });   
    
$("#fecha").change(function () {
	selectColor();
});     
llenarComboServicio();
$("#servicioA").click(function () {
alert($(this).attr("id"))
});

</script>
@endsection

@section('cuerpo')
@if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif 
<section class="site-section bg-white " id="testimonials-section" data-aos="fade">
    <div class="container" style="margin-block: 1%">
    
        <div class="row">
			<div hidden  class="card align-self-center" style="width:300px">
                <div class="card-header btn-succses">
                    <div class="card-title text-center">
                        <p class="card-text">
                        <h4>Datos Cliente</h4><input type="hidden" value="" id="idCliente" name="idCliente" />
                        </p>
                        <select id="horass" >
							@foreach ($citas as $cita)
                            <option value="{{$cita->hour}}">{{$cita->hour}}</option>
							@endforeach
						</select>
						<select id="fechass">
							@foreach ($citas as $cita)
                            <option value="{{$cita->datequotes}}">{{$cita->datequotes}}</option>
							@endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3"> </div>
            <div class="card" style="margin-block: 1%">
                <div class="card-header btn-succses">
                    <div class="card-title text-center">
                        <div class="card-header bg-light">
                            <h5>DATOS CITA MÉDICA</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <form action="{{route('registrarcitanueva')}}" method="POST" class="form-box" id="forcita">
							@csrf
                            @method('put')
                            <label class="text-black">Seleccione la especialidad</label>
                            <select class="form-control" id="selectlistauno" name="selectlistauno">
    
                                @foreach ($especialidades as $especialidad)
        
                                <option value="{{$especialidad->id}}">{{$especialidad->name}}</option>
                                @endforeach
                            </select>
                            <label class="text-black" for="exampleFormControlSelect2">Seleccione el médico</label>
                            <div class="form-group" >
                                <select class="form-control"  name="selectlistados" id="selectlistados">
                                </select>
                            </div>
                            <label for="tipocita">Seleccione el tipo de la cita</label>
							<div class="form-group">
                                <select class="form-control" name="tipocita" id="tipocita">
									<option value="general">Cita general</option>
                                        <option value="Checkeop">Chequeo de examenes</option>
                                </select>
                            </div>
                            <label class="text-black" for="fecha">Seleccione la fecha</label>
                            <div class="form-group">
                                <input type="date" id="fecha" min="<?php $hoy=date("Y-m-d"); echo $hoy;?>" name="fecha" class="form-control">
                            </div>

                            <label hidden class="text-black" for="fecha">Horario del doctor</label>
                            <div class="form-group" name="horadoc" id="horadoc">
                            </div>

                            <label hidden class="text-black" for="fecha">horassss</label>
                            <div class="form-group">
                                <input hidden type="text"  name="horacomparar" id="horacomparar" value="">
                            </div>


                            <div class="form-group">
                                <label class="text-black" for="exampleFormControlSelect2">Seleccione hora</label>
								<select hidden multiple class="form-control text-black dropdown-secondary" id="inf" size="8">
                                </select>

                                <select multiple class="form-control text-black dropdown-secondary" name="hora" id="horario" size="8">
                                  
                                </select>
                            </div>
                            <input id="valor" type="hidden" value="false" name="valor"> 
                            <div class="alert alert-info" role="alert">
                                Estimado(a) cliente una vez agendada tu cita no podras modificar los datos de la misma
                            </div>
                            <input  type="hidden" value="{{$pasiente->id }}" name="paciente" id="paciente">
                            <input id="valor" type="hidden" value="0" name="valor" >
                            <div class="form-group">
                                <input type="submit" id="bcita" class="btn btn-primary" value="Generar Cita" id="genCit">
                                <a href="{{asset('homecliente')}}" class="btn btn-danger ">Volver</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection