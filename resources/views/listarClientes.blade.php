
@extends('layouts.app')

@section('navegadorapp')
@endsection 

@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
        <li> <a href="homemedico">
          Incio</a></li>
          <li> <a href="homemedico">
            Citas</a></li>
        <li> <a href="listarClientes"  >
          Pacientes</a></li>
        
        <li></li>
      </ul>
  </div>
       
  
</div>

<!-- Fin sidebar -->

  

@endsection
@section('cuerpo')
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

  <!-- Content -->
  <div class="container" style="padding-block: 5%">

    <h2>Pacientes 
      
      </h2>
      <div class="panel">
           

      <table class="table table-striped table-bordered table-hover" id="tablas"> 
      <thead>
          <tr>
            <th>Cedula</th><th>Nombre</th><th>Apellido</th><th>Teléfono</th><th>Cliente</th><th>Estado de la cita</th><th>Acción</th>
          </tr>
      </thead>
      <tbody>
            @foreach ($clientes as $cliente) 
              @foreach ($personas as $persona)
              @foreach ($pasiente as $item)
              @foreach ($personados as $pero)
              @foreach ($doctor as $doc)
                 @foreach ($cita as $ci)
                  @foreach ($userdoc as $userdoctor)
              @if ($item->people_id == $persona->id && $item->client_id == $cliente->id
              && $item->id == $ci->patient_id && $doc->id == $ci->doctor_id  &&
              Auth::user()->id == $userdoctor->user_id && $userdoctor->doctor_id ==$ci->doctor_id
              && $pero->id == $cliente->people_id)
              <tr>
                <td>{{$persona->cedula }}</td>
                <td>{{$persona->name }}</td>
                <td>{{$persona->surname}} </td> 
                <td>{{$persona->phone}}</td>
                <td>{{$pero->name}} {{$pero->surname}} </td> 
                <td>
                  @if ($ci->state == 1)
                    Paciente atendido 
                  @else  
                    Paciente sin atender 
                  @endif
                </td>
                <td>
                
                  <div class="btn-group">
                  <button type="button" class="btn btn-success">Visualizar</button>

                  <button type="button" class="btn btn-success dropdown-toggle"
                          data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Desplegar menú</span>
                  </button>
                
                    <ul class="dropdown-menu" role="menu">
                      <li><a type="button" href="{{route('verhistorialmedicocliente',$persona->external_id)}}"  class="btn btn-success" >
                        Historial/Exámenes
                      </a></li>
                     
                      
                    </ul>
                  </div>
                 
                </td>
              </tr>
            
            @endif
            @endforeach 
            @endforeach
            @endforeach 
            @endforeach  
              @endforeach
              @endforeach
            @endforeach
        </tbody>
      </table>
  
 </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Historial Médico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario" method="POST" action="{{route('agregarhistorial',$persona->external_id)}}">
                    @csrf
                    @method('put')
                    
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                        </ul>
                        <input type="text" class="form-control" id="d" name="d" value="{{$persona->external_id}}">
                        <!-- Tab panes -->

                              <div class="form-group">
                                  <label for="enf" class="control-label">Enfermedades:</label>
                                  <input type="text" class="form-control" id="enf" name="enf">
                              </div>
                              <div class="form-group">
                                  <label for="enf_her" class="control-label">Enfermedades hederitarias:</label>
                                  <input type="text" class="form-control" id="enf_her" name="enf_her">
                              </div>
                              <div class="form-group">
                                  <label for="hab" class="control-label">Habitos:</label>
                                  <input type="text" class="form-control" id="hab" name="hab">
                              </div>
                              
                          </div>

                   
                    </div>
                    <div class="modal-footer">                                    
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
      </div>
    </div>
  </div>

 


@endsection
@section('scripts')
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection




