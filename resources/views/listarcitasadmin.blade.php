

@extends('layouts.app')

@section('navegadorapp')
@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
          <li class="active"><a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
          <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
          <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
          <li><a href="{{route('listaradmin')}}">Clientes</a></li>
          <li> <a  href="{{route('especialida')}}">Especialidad</a>
        </li>
          <li><a  href="{{route('salirAdmin')}}">Cerrar sesión</a></li>
      </ul>
  </div>
</div>
  <!-- Start header -->
  

<!-- Header End -->
@section('cuerpo')

@if ($datos == false)
<p class="h3 text-center">No hay citas registradas</p>
@else
    


<div class="container" style="padding-block: 5%">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <h2>Lista de citas
    </h2>
  
    <div class="row">
      <div class="col">
        <table class="table table-striped table-bordered table-hover" id="tablas">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Hora</th>
              <th>Paciente</th>
              <th>Cliente</th>
              <th>Doctor</th>
              <th>Especialidad</th>
              <th>Estado de la cita</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
             @foreach ($cita as $cit)
                 @foreach ($doctore as $doctor)
                     @foreach ($paciente as $paci)
                         @foreach ($persona as $per)
                         @foreach ($pe as $p)
                         @foreach ($especialidad as $esp)
                         @foreach ($cliente as $clie)
                          @foreach ($personados as $item)
                             @if ($cit->doctor_id == $doctor->id && $cit->patient_id == $paci->id && 
                             $paci->people_id == $per->id && $p->id == $doctor->people_id && $cit->specialtie_id == $esp->id
                             && $paci->client_id == $clie->id && $clie->people_id == $item->id)
                             <tr>
                              <td>{{$cit->datequotes}}</td>
                              <td>{{$cit->hour}}</td>
                              <td>{{$per->name}} {{$per->surname}}</td>
                              <td>{{$item->name}} {{$item->surname}}</td>
                              <td>{{$p->name}} {{$p->surname}}</td>
                              <td>{{$esp->name}} </td>
                              @if ($cit->state == 1)
                                <td>Activo</td>
                                @else
                                <td>Inactivo</td>
                              @endif
                              
                                @if ($cit->state == 1)
                                <form method="POST" action="{{route('cancelarcita',$cit->external_id)}}">
                                  @csrf
                                <td><button type="submit" class="btn btn-success">
                                  Cancelar Cita
                                </button>
                              </form>
                              </td>
                                @else
                                <td>
                                  <a type="text" >
                                    Cita cancelada
                                  </a>
                                </td>
                                @endif
                             </tr>
                              @endif
                              @endforeach   
                              @endforeach
                              @endforeach
                              @endforeach
                         @endforeach
                     @endforeach
                 @endforeach
             @endforeach
          </tbody>
        </table>
      </div>
    </div>

    @endif
@endsection


  <!-- Fin Navbar -->
@endsection
@section('scripts')
<script language="javascript" type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection