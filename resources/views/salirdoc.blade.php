

@extends('layouts.app')

@section('navegadorapp')
    
@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
    <div class="logo">
      <h2><a href="#">Kulo</a></h2>
    </div>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="menu">
      <ul class="nav navbar-nav">
        
        <li> <a href="homemedico">
            Citas</a></li>
        <li> <a href="listarClientes">
            Pacientes</a></li>
            <li><a  href="{{route('salirdoc')}}">Cerrar sesión</a></li>
        
      </ul>
    </div>
  
    
  </div>
  
<!-- Fin sidebar -->
@section('cuerpo')



<div style="padding-block: 5%; padding-left: 50%">
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
          Cerrar sesión
        </a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</div>


@endsection


  <!-- Fin Navbar -->
@endsection