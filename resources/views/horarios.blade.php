@extends('layouts.app')
@section('navegadorapp')
@endsection
@section('navegador')
<!-- Sidebar -->
<!-- Fin sidebar -->


   <!-- Fin Navbar -->
@endsection
@section('scripts')
<script src="{{asset('assets/metodos.js')}}"></script>

@endsection

@section('cuerpo')
<div class="container" style="margin-block: 1%">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Registro de Horarios</div>

                <div class="card-body">
                    <form  id="formulario" method="POST" action="{{route('guardarhorario')}}"> 
    
                        @csrf
                        <div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="form-group">
                                        <label for="horaentrada" class="control-label">Hora de entrada:</label>
                                        <select id="horaentrada" name="horaentrada" class="form-control">
                                          <option value="8:00" id="h1">8:00</option>
                                          <option value="8:30" id="h2">8:30</option>
                                          <option value="9:00" id="h3">9:00</option>
                                          <option value="9:30" id="h4">9:30</option> 
                                          <option value="10:00" id="h5">10:00</option>
                                          <option value="10:30" id="h6">10:30</option>
                                          <option value="11:00" id="h7">11:00</option>
                                          <option value="11:30" id="h8">11:30</option>
                                          <option value="12:00" id="h9">12:00</option>
                                          <option value="16:00" id="h12">16:00</option>
                                          <option value="16:30" id="h13">16:30</option>
                                          <option value="17:00" id="h14">17:00</option>
                                          <option value="17:30" id="h15">17:30</option>
                                          <option value="18:00" id="h16">18:00</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="horasalida" class="control-label">hora de salida:</label>
                                      <select id="horasalida" name="horasalida" class="form-control">
                                        <option value="8:00" id="h1">8:00</option>
                                        <option value="8:30" id="h2">8:30</option>
                                        <option value="9:00" id="h3">9:00</option>
                                        <option value="9:30" id="h4">9:30</option> 
                                        <option value="10:00" id="h5">10:00</option>
                                        <option value="10:30" id="h6">10:30</option>
                                        <option value="11:00" id="h7">11:00</option>
                                        <option value="11:30" id="h8">11:30</option>
                                        <option value="12:00" id="h9">12:00</option>
                                        <option value="16:00" id="h12">16:00</option>
                                        <option value="16:30" id="h13">16:30</option>
                                        <option value="17:00" id="h14">17:00</option>
                                        <option value="17:30" id="h15">17:30</option>
                                        <option value="18:00" id="h16">18:00</option>
                                      </select>
                                  </div>
                                </div>
                            </div>
                            <input id="external" name= "external" value="{{$doctor->external_id}}" hidden />
                        </div>
                        <div class="modal-footer">                                    
                          <a href="{{asset('')}}" class="btn btn-danger ">Volver</a>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                            
                    </form>
                
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection