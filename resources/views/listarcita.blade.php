 
@extends('layouts.app')

@section('navegadorapp')
@endsection 

@section('navegador') 

<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#">Kulo</a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
        
          <li><a href="{{asset('homecliente')}}" >
            Inicio</a></li>
        <li> <a href="{{route('citapa', Auth::user()->external_id)}}"  >
            Citas</a></li>
        <li> <a href="{{route('paciente',Auth::user()->external_id)}}">
            Pacientes</a></li>
            <li> <a class="dropdown-item" href="{{route('modcliente',Auth::user()->external_id) }}">
              Modificar datos
           </a></li>
        
  </div>
       
  
</div>

<!-- Fin sidebar -->


   <!-- Fin Navbar -->
@endsection
@section('cuerpo')

@if ($vacio == true)
<a type="text" style="font-size: 35px; margin-left: 15%">Aun no hay registros de Citas</a>
@else
    



<div class="container" style="padding-block: 5%">
  <h2>Lista de Citas
  </h2>
  <div class="row">
    <div class="col">
      <table class="table table-striped table-bordered table-hover" id="tablas">
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Paciente</th>
            <th>Doctor</th>
            <th>Especialidad</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($cita as $item)
            @foreach ($persona as $itemdoc)
                @foreach ($doctor as $itemtres)
                @foreach ($pasiente as $ite)
                @foreach ($perso as $it)
                  @foreach ($especialidad as $espe)
                      
                  
                @if ($itemtres->id == $item->doctor_id && $item->patient_id == $ite->id && $itemtres->people_id == $itemdoc->id && 
                $cliente->id == $ite->client_id && $it->id == $ite->people_id && $espe->id == $item->specialtie_id)
                <tr>
                  <td>{{ $item->datequotes }}</td>
                  <td>{{ $item->hour }}</td>
                  <td>{{$it->name}} {{$it->surname}}</td>
                  <td>{{ $itemdoc->name }} {{$itemdoc->surname}}</td>
                  <td>{{ $espe->name }}</td>
                </tr>
              @endif
              @endforeach
                @endforeach
                @endforeach
                @endforeach
            @endforeach
          @endforeach
           
        </tbody>
      </table>
    </div>
  </div>
  
@endif
@endsection
@section('scripts')
<script language="javascript" type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection