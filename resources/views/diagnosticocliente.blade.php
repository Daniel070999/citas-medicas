

@extends('layouts.app')

@section('navegadorapp')
    
@endsection
@section('navegador')


 
  @section('cuerpo')
  @if ($datos == false)
            <a type="text" style="font-size: 35px; margin-left: 15%">No hay exmenes</a>
  @else
    <a href="{{asset('/')}}" style=" margin-left:  90%;margin-block: 1%;" type="button" class="btn btn-outline-warning" >Cancelar</a>
    <div class="container" style="padding-block: 5%">
      <h2>Diagnósticos
      </h2> 
      <div class="row">
        <div class="col"> 
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>hora</th>
                <th>fecha</th>
                <th>tipo de examen</th>
                <th>Diagnóstico</th>
                <th>Resultado</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody> 
                    @foreach ($mediExa as $diagnostic)
                    <tr>
                      <td>{{$diagnostic->hour}}</td>
                      <td>{{$diagnostic->fechaexamen}}</td>
                      <td>{{$diagnostic->typeExa}}</td>
                      <td>{{$diagnostic->diagnosis}}</td>
                      <td>{{$diagnostic->outcome}}</td>
                      <td hidden>{{$da = $diagnostic->external_id}}</td>
                      <td>
                        <a href="" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modaldos">Historial Médico</a>
                        </td>
                       
                    </tr>
                    @endforeach
            </tbody>
          </table>
        </div>
      </div>   
      
      
      <div class="w-100">
        <div class="modal fade" tabindex="-1" role="dialog" id="modaldos">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header primary">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Registro de horario</h4>
                      
                  </div>
                  <div class="modal-body">
                  
                    <form id="formulario" method="POST" action="{{route('agregarresultado',$da)}}">
                      @csrf
                      @method('PUT')
                      <div>
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                          </ul>
                          <!-- Tab panes -->
      
                                <div class="form-group">
                                    <label for="enf" class="control-label">Diagnóstico:</label>
                                    <input type="text" class="form-control" id="enf" name="enf">
                                </div>
                                <div class="form-group">
                                    <label for="enf_her" class="control-label">Resultados:</label>
                                    <input type="text" class="form-control" id="enf_her" name="enf_her">
                                </div>
          
                                
                            </div>
                      </div>
                      <div class="modal-footer">                                    
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-primary">Guardar</button>
                      </div>
                  </form>
                  
                  </div>
      
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- fin modal -->
    @endif
  @endsection
@endsection


