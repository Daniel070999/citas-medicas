

@extends('layouts.app')

@section('navegadorapp')
    
@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#">Kulo</a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
        <li> <a href="{{route('homemedico')}}">
          Incio</a></li>
          <li> <a href="{{route('homemedico')}}">
            Citas</a></li>
        <li> <a href="{{route('listarClientes')}}"  >
          Pacientes</a></li>
        
        <li></li>
      </ul>
  </div>
       
  
</div>

<div class="w-100">
  <style>
    
    </style>
      
  
 <!-- Navbar -->
 
  @section('cuerpo')

  <div class="w-100">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header primary">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Registro de horario</h4>
                  
              </div>
              <div class="modal-body">
              
                <form id="formulario" method="POST" action="{{route('agregarresultado',$da)}}">
                  @csrf
                  @method('PUT')
                  <div>
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                      </ul>
                      <!-- Tab panes -->
  
                            <div class="form-group">
                                <label for="enf" class="control-label">Diagnóstico:</label>
                                <input type="text" class="form-control" id="enf" name="enf">
                            </div>
                            <div class="form-group">
                                <label for="enf_her" class="control-label">Resultados:</label>
                                <input type="text" class="form-control" id="enf_her" name="enf_her">
                            </div>
      
                            
                        </div>
                  </div>
                  <div class="modal-footer">                                    
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
              </form>
              
              </div>
  
          </div><!-- /.modal-content -->
  </div><!-- /.modal -->
  <!-- fin modal -->

  @endsection
@endsection


