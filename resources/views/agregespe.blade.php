@extends('layouts.app')
@section('navegadorapp')
@endsection
@section('navegador')
<!-- Sidebar -->
<!-- Fin sidebar -->


   <!-- Fin Navbar -->
@endsection
@section('scripts')
<script src="{{asset('assets/metodos.js')}}"></script>

@endsection

@section('cuerpo')
<div class="container" style="margin-block: 1%">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Registro de especialidad</div>

                <div class="card-body">
                    <form  id="formulario" method="POST"action="{{route('agreespe')}}"> 
    
                        @csrf
                        <div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                  <div class="form-group">
                                    <label for="Fecha">Seleccione su especialidad:</label>
                                      <select name="especialidad" id="especialidad" class="form-control" aria-describedby="espeHelp">
                                        @foreach ($especialida as $item)
                                          @if ($item->active == 1)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                          @endif
                                        @endforeach  
                                    
                                          
                                      </select> 
                                      <input  hidden type="text" value="{{$doctor->id }}" id="doctor" name="doctor">
                                               
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">                                    
                          <a href="{{asset('/')}}" type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</a>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                            
                    </form>
                
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection