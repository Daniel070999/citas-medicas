@extends('layouts.app')

@section('cuerpo')
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
    <div class="logo">
        <h2><a href="#">Kulo</a></h2>
    </div>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="menu">
        <ul class="nav navbar-nav">
            <li class="active">
           <!-- Authentication Links -->
           @guest
                        
                        
           @if (Route::has('register'))
           
                   <a class="nav-link" href="{{ route('home') }}">[Regresar]</a>
               
           @endif
       @else
           class="nav-item dropdown">
               <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                   {{ Auth::user()->name }}
               </a>

               <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                   <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                       {{ __('Logout') }}
                   </a>

                   <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                       @csrf
                   </form>
               </div>
          
       @endguest
            </li>
        </ul>
    </div>
  </div>
    <!-- Start header -->
    <main class="py-4">
        @yield('content')
    </main>
  
  <!-- Header End -->
@endsection
@section('navegadorapp')

  

@endsection

<div class="container" style="margin-block: 1%">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
               
                <div class="card-body">
                   
    
                    <form  id="formulario" method="POST" action="{{route('registrarcliente')}}"> 
    
                        @csrf
                        <div style="padding-left:150px; padding-right: 150px">
                            <ul class="nav nav-tabs">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">DATOS GENERALES</a></li>
                               
                            </ul>
                            <div class="tab-content" />
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="form-group">
                                    <label for="cedula">Cédula</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Ingrese su cédula">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nombres">Nombres:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese sus nombres">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="apellidos">Apellidos:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Ingrese sus apellidos">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="direccion">Dirección:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese su dirección">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Teléfono:</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control " id="telefono" name="telefono" placeholder="Ingrese su teléfono">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Género:</label>
                                    <select name="genero" id="genero" class="form-control" aria-describedby="espeHelp">
                                        <option value="masculino">Masculino</option>
                                        <option value="femenino">Femenino</option>
                                    </select>            
                                </div>
                                <div class="form-group">
                                    <label for="fecha">Fecha de nacimiento:</label>
                                    <div class="input-group">
                                        <input type="Date" class="form-control " id="fecha" name="fecha" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="historia">Historia Clínica:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="historia" name="historia" readonly>
                                    </div>
                                </div>
                                <label for="email">Email:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese su email">
                                </div>
                
                                <div class="form-group">
                                <label for="password">Clave:</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Ingrese su password">
                                </div>
                            </div>
                                <label name="acceso" id="acceso" hidden>
                                    <input type="text" value="cliente" id="acceso" name="acceso">
                                </label>            
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-outline-primary">Registrar</button>
                                <a href="{{asset('login') }}" class="btn btn-outline-secondary">Cancelar</a>
                            </div>
                            
                    </form>
                    <select hidden name="cedulas" id="cedulas">
                    @foreach ($cedula as $ced)
                        <option value="{{$ced->cedula}}">{{$ced->cedula}}</option>
                     @endforeach
                     <input hidden type="text" name="long" id="long" value="{{$tamanio}}">
                    </select>

                    <select hidden name="correos" id="correos">
                        @foreach ($correo as $corr)
                            <option value="{{$corr->email}}">{{$corr->email}}</option>
                         @endforeach
                         <input hidden type="text" name="tamaniocorreos" id="tamaniocorreos" value="{{$tamaniocorreo}}">
                    </select>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('scripts')
<script>
$(document).ready(function () {
    $historiClinica = Math.floor((Math.random() * (10000 - 10)) + 10);
$("#historia").val("HI-"+$historiClinica);

var cedu = recorrerSelect(document.getElementById("cedulas"));
var longitud = $('#long').val();

$('#cedula').change(function(){
    var ced = $('#cedula').val();
    for (i = 0 ;i <= longitud; i++){
        if (cedu[i] == ced) {
            Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: 'Esta identificación ya se encuentra registrada '
                });
            
        $('#cedula').val("");
	}
    }
});

var cor = recorrerSelect(document.getElementById("correos"));
var longcor = $('#tamaniocorreos').val();

$('#email').change(function(){
    var correoss = $('#email').val();
    for (i = 0 ;i <= longcor; i++){
        if (cor[i] == correoss) {
            Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: 'El correo ya esta registrado, porfavor ingrese otro '
                });
            
        $('#email').val("");
	}
    }
});


});
</script>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
