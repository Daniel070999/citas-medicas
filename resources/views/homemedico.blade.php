@extends('layouts.app')

@section('navegadorapp')

@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
    <h2><a href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
    <ul class="nav navbar-nav">

      <li> <a href="homemedico">
          Citas</a></li>
      <li> <a href="listarClientes">
          Pacientes</a></li>

            <li><a  href="{{route('salirdoc')}}">Cerrar sesión</a></li>
    </ul>
  </div>

  
</div>

<!-- Fin sidebar -->

<div class="w-100">


  <!-- Fin Navbar -->
  @section('cuerpo')
  <div class="container" style="padding-block: 5%">
    <h2>Citas
      <!--<a href="{{asset('createmedico')}}" class="btn btn-primary" style="margin-left: 5%">Nuevo</a> -->
    </h2>
    <div class="container">
   
    <div class="row">
      <div class="col">
        <table class="table table-striped table-bordered table-hover" id="tablas">
          <thead>
            <tr>
              <th>Cedula paciente</th>
              <th>Fecha</th>
              <th>Hora</th>
              <th>Tipo</th>
              <th>Paciente</th>
              <th>Cliente</th>
              <th>Especialidad</th>
              <th>Estado Cita</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            
            @foreach ($citas as $cita)
              @foreach ($doctores as $doctor)
                @foreach ($userdoctores as $userdoctor)
                  @foreach ($espe as $esp)
                    @foreach ($pe as $per)
                      @foreach ($pa as $paci)
                      @foreach ($cliente as $cli)
                        @foreach ($personados as $item)
                        @if ( Auth::user()->id == $userdoctor->user_id && $userdoctor->doctor_id ==
                            $cita->doctor_id && $doctor->id == $cita->doctor_id && $cita->patient_id == $paci->id && 
                           $paci->people_id == $per->id && $esp->id == $cita->specialtie_id && $paci->client_id == $cli->id &&
                           $cli->people_id == $item->id)
                          <tr>
                            <td>{{$per->cedula}}</td>
                            <td>{{$cita->datequotes}}</td>
                            <td>{{$cita->hour}}</td>
                            <td>{{$cita->quotetype}}</td>
                            <td>{{$per->name}} {{$per->surname}}</td>
                            <td>{{$item->name}} {{$item->surname}}</td>
                            <td>{{$esp->name}}</td>
                            
                              @if ($cita->state == 1)
                              <form method="POST" action="{{route('desactivarcita',$cita->external_id)}}">
                                @csrf
                              
                              <td><button type="submit" class="btn btn-warning">
                                Paciente sin atender 
                                
                              </button>
                              </td>
                            </form>
                              @else
                              <td><button type="submit" class="btn btn-success">
                                 
                                Paciente atendido  
                              </button> 
                            </td>
                              @endif
                            
                            <td>
                              <div class="btn-group">
                                <a type="button" href="{{route('agregarhist',[$per->external_id ,$cita->external_id])}}"  class="btn btn-primary" >
                                  Atención al paciente 
                                </a>
                                </div>
                                <div class="btn-group">
                                    <a type="button" href="{{route('verhistorialmedicocliente',$per->external_id)}}"  class="btn btn-success" >
                                      Ver historial
                                    </a>

                                </div>
                            </td>
                           </tr>
                          @endif
                          @endforeach
                          @endforeach
                        @endforeach
                      @endforeach
                    @endforeach
                   @endforeach
                @endforeach
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    </div>
  </div>
    
    @endsection
    @endsection
  


@section('scripts')
<script language="javascript" type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection