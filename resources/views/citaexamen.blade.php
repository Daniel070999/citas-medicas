@extends('layouts.app')

@section('navegadorapp')
@endsection 

@section('navegador')

@endsection
@section('cuerpo')
<section class="banner-area py-7" id ="hola">
    <!-- Content -->
    <div class="container" style="padding-block: 5%">
    <div class="panel panel-primary">
      <h2>Pacientes 
        
        </h2>
        <form id="formulariodos" method="POST" action="{{route('agregardiagnostico',$persona->external_id)}}">
            @csrf
            @method('put')
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                      <div class="form-group">
                        <select name="tipoExa" id="tipoExa" class="form-control" aria-describedby="espeHelp">
                            <option value="Hemograma completo">Hemograma completo</option>
                            <option value="Urinálisis completo">Urinálisis completo</option>
                            <option value="Heces por parásito, sangre oculta">Heces por parásito, sangre oculta</option>
                            <option value="Perfil lipídico: Colesterol, LDL; HDL; triglicérido">Perfil lipídico: Colesterol, LDL; HDL; triglicérido</option>
                        </select>            
                        <small id="espeHelp" class="form-text text-muted">seleccione el tipo de examen.</small>
                    </div>
                    <div class="form-group">
                      <input type="date" id="fechaex" min="<?php $hoy=date("Y-m-d"); echo $hoy;?>" name="fechaex" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="text-black" for="exampleFormControlSelect2">Seleccione hora</label>
                    <select hidden multiple class="form-control text-black dropdown-secondary" id="inf" size="8">
                    </select>
                      <select multiple class="form-control text-black dropdown-secondary" name="hora" id="horario" size="8">
                          <option value="8:00-8:30" id="h1">8:00-8:30</option>
                          <option value="8:30-9:00" id="h2">8:30-9:00</option>
                          <option value="9:00-9:30" id="h3">9:00-9:30</option>
                          <option value="9:30-10:00" id="h4">9:30-10:00</option> 
                          <option value="10:00-10:30" id="h5">10:00-10:30</option>
                          <option value="10:30-11:00" id="h6">10:30-11:00</option>
                          <option value="11:00-11:30" id="h7">11:00-11:30</option>
                          <option value="11:30-12:00" id="h8">11:30-12:00</option>
                          <option value="12:00-12:30" id="h9">12:00-12:30</option>
                          <option value="almuerzo" disabled=""  >Hora de almuerzo</option>
                          <option value="16:00-16:30" id="h12">16:00-16:30</option>
                          <option value="16:30-17:00" id="h13">16:30-17:00</option>
                          <option value="16:00-16:30" id="h14">17:00-17:30</option>
                          <option value="16:30-17:00" id="h15">17:30-18:00</option>
                          <option value="16:00-16:30" id="h16">18:00-18:30</option>
                      </select>
                  </div>
                        <div class="form-group">
                          <label for="descripcion" class="control-label">Descripción:</label>
                          <div class="modal-body">
                            <textarea class="form-control col-xs-12" id="descripcion" name="descripcion"></textarea>
                        </div>
                       
                      </div>
                      
                    </div>
                </div>
            </div>
            <div class="modal-footer">                                    
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>
</section>

@endsection