

@extends('layouts.app')

@section('navegadorapp')
    
@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
          <li class="active"><a <a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
          <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
          <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
          <li><a href="{{route('listaradmin')}}">Clientes</a></li>
          <li><a  href="salirAdmin">Cerrar sesión</a></li>
      </ul>
  </div>
</div>
<!-- Fin sidebar -->
@section('cuerpo')



<div style="padding-block: 5%; padding-left: 50%">
    <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">Cerar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</div>


@endsection


  <!-- Fin Navbar -->
@endsection
