

@extends('layouts.plantilla')

@section('title', 'Cliente ver'.$persona->nombre)

@section('navegador')
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="col-md-12">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
          <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="{{route('listarClientes')}}">Regresar</a></li>
          </ul>
          <ul class="nav navbar-nav">
            <li class="active"><a href="{{route('modificar',$persona->id )}}">Modificar</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
 
@endsection

@section('fondo')
<h1>hola {{$persona->nombre}}</h1>
@endsection

@section('cuerpo')



@endsection
