@extends('layouts.app')
@section('navegadorapp')
@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
    <div class="logo">
        <h2><a href="#">Kulo</a></h2>
    </div>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="menu">
        <ul class="nav navbar-nav">
            <li class="active"><a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
            <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
            <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
            <li><a href="{{route('listaradmin')}}">Clientes</a></li>
            <li> <a  href="{{route('especialida')}}">Especialidad</a>
          </li>
            <li><a  href="{{route('salirAdmin')}}">Cerrar sesión</a></li>
        </ul>
    </div>
  </div>
    <!-- Start header -->
    
  
  <!-- Header End -->
@endsection

@section('cuerpo')

@if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif 
<section class="site-section bg-white " id="testimonials-section" data-aos="fade">
    <div class="container" style="margin-block: 1%">
        <div class="row">
            <div class="col-md-3"> </div>
            <div class="card" style="margin-block: 1%">
                <div class="card-header btn-succses">
                    <div class="card-title text-center">
                        <div class="card-header bg-light">
                            <h5>DATOS DEL PACIENTE</h5>
                        </div>
                    </div>
                </div>

              

                <div class="card-body">
                    <div class="form-group">
                            <form id="formulario" method="POST" action="{{route('pasienteCre')}}">
                                @csrf
                                <div>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos cita</a></li>
                                    </ul>
            
                                    <!-- Tab panes --> 
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <div class="form-group">
                                                <label for="cedula">Cédula</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Ingrese su cédula">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nombres">Nombres:</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese sus nombres">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="apellidos">Apellidos:</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Ingrese sus apellidos">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="direccion">Dirección:</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese su dirección">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">Teléfono:</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control " id="telefono" name="telefono" placeholder="Ingrese su teléfono">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">Género:</label>
                                                <select name="genero" id="genero" class="form-control" aria-describedby="espeHelp">
                                                    <option value="masculino">Masculino</option>
                                                    <option value="femenino">Femenino</option>
                                                </select>            
                                            </div>
                                            <div class="form-group">
                                                <label for="fecha">Fecha de nacimiento:</label>
                                                <div class="input-group">
                                                    <input type="Date" class="form-control " id="fecha" name="fecha" >
                                                </div>
                                            </div>
                                        </div>
    
                                            <label name="acceso" hidden >
                                                <input type="text" value="{{$cliente->external_id}}" id="cliente" name="cliente">
                                            </label>            
                                       
                                        
            
                                    </div>
                                </div>
                                <div class="modal-footer">                                    
                                    <a href="{{asset('listaradmin')}}" class="btn btn-danger ">Volver</a>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </form>
                            <select hidden name="cedulas" id="cedulas">
                                @foreach ($cedula as $ced)
                                    <option value="{{$ced->cedula}}">{{$ced->cedula}}</option>
                                 @endforeach
                                 <input hidden type="text" name="long" id="long" value="{{$tamanio}}">
                                </select>
            
                                <select hidden name="correos" id="correos">
                                    @foreach ($correo as $corr)
                                        <option value="{{$corr->email}}">{{$corr->email}}</option>
                                     @endforeach
                                     <input hidden type="text" name="tamaniocorreos" id="tamaniocorreos" value="{{$tamaniocorreo}}">
                                </select>
                       
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
$(document).ready(function () {
    $historiClinica = Math.floor((Math.random() * (10000 - 10)) + 10);
$("#historia").val("HI-"+$historiClinica);

var cedu = recorrerSelect(document.getElementById("cedulas"));
var longitud = $('#long').val();

$('#cedula').change(function(){
    var ced = $('#cedula').val();
    for (i = 0 ;i <= longitud; i++){
        if (cedu[i] == ced) {
		alert("cedula repetida");
        $('#cedula').val("");
	}
    }
});

var cor = recorrerSelect(document.getElementById("correos"));
var longcor = $('#tamaniocorreos').val();

$('#email').change(function(){
    var correoss = $('#email').val();
    for (i = 0 ;i <= longcor; i++){
        if (cor[i] == correoss) {
		alert("El correo ya esta registrado, porfavor ingrese otro");
        $('#email').val("");
	}
    }
});


});
</script>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
@endsection