<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Digital Agency Template</title>
    <link href="https://fonts.googleapis.com/css?family=Croissant+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('clas/nicepage.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('clas/Casa.css')}}" media="screen">
  
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
     <!---Css para las Notificaciones ---->
     <link rel="stylesheet" href="{{ asset('alerta/css/Lobibox.min.css') }}">
     <link rel="stylesheet" href="{{ asset('alerta/css/notifications.css') }}">

     
</head>

<body>
    <div class="wrapper">
        <header class="header">
            <div class="blue">
                <img src="{{asset('assets/img/header-shepe-blue.png')}}" alt="">
            </div>
            <div class="white">
                <img src="{{asset('assets/img/header-shepe-white.png')}}" alt="">
            </div>
            <div class="container">
                <img class="shepe1" src="{{asset('assets/img/shepe1.png')}}" alt="">
                <img class="shepe2" src="{{asset('assets/img/shepe2.png')}}" alt="">
                <img class="shepe3" src="{{asset('assets/img/shepe2.png')}}" alt="">
                <img class="shepe4" src="{{asset('assets/img/shepe2.png')}}" alt="">
                <img class="shepe5" src="{{asset('assets/img/shepe1.png')}}" alt="">
                <img class="shepe6" src="{{asset('assets/img/shepe2.png')}}" alt="">
                <div class="row">
                    @section('navegador')
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="logo">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="menu">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#">About</a></li>
                            </ul>
                        </div>
                    </div>
                        @show
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-text">
                           
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="another-text">
                            <h3 style="background-color: transparent">Hospital Nuestra Familia</h3>
                           
                        </div>
                    </div>
                    
                </div>
                
            </div>
           
        </header>
        <header>
            <div class="another-text">
            @section('cuerpo')
            @show
            </div>
        </header>
        
        <section class="development">
        
            <div class="blue">
                <img src="{{asset('assets/img/development-shepe-blue.png')}}" alt="">
            </div>
            <div class="white">
                <img src="{{asset('assets/img/development-shepe-white.png')}}" alt="">
            </div>
            
        </section>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="footer-icon">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="footer-text">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="footer-text-single">

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="footer-text-single">

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="footer-text-single">

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <div class="footer-text-single">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{asset('assets/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/active.js')}}"></script>
    <!-- Libreria js para las Notificaciones -->
    <script src="{{ asset('alerta/js/Lobibox.js') }}"></script>
    <script src="{{ asset('alerta/js/notification-active.js') }}"></script>
    @yield('scripts')
</body>

</html>