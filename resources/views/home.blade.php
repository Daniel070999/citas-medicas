@extends('layouts.app')

@section('navegadorapp')

@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a  class= "text-left"href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
          <li class="active"><a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
          <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
          <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
          <li><a href="{{route('listaradmin')}}">Clientes</a></li>
          <li> <a  href="{{route('especialida')}}">Especialidad</a>
        </li>
          <li><a  href="{{route('salirAdmin')}}">Cerrar sesión</a></li>
      </ul>
  </div>
</div>
  <!-- Start header -->
  

<!-- Header End -->
@section('cuerpo')

<!-- Button trigger modal -->
<div class="container" style="padding-block: 5%">

  <h2>Lista de Médicos
    <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modal" style="margin-left: 5%">Nuevo</a>
  </h2>
 
    
  </div>
  <div class="row">
    <div class="col">

      @if ($verificar == true)
      <p class="h3 text-center">No hay doctores registrados</p>

      @else

    
        
         

  <div class="container">
   <table class="table table-striped table-bordered table-hover" id="tablas">
        <thead>
          <tr>
            <th>Cedula</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Especialidad</th>
            <th>Horario</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($user as $item)
          @foreach ($userdoctores as $itemdoc)
          @foreach ($doctore as $itemdos)
          @foreach ($persona as $itemtres)
          

          @if ($itemtres->id == $itemdos->people_id && $item->id == $itemdoc->user_id && $itemdos->id ==
          $itemdoc->doctor_id  )

          <tr>
            <td>{{ $itemtres->cedula }}</td>
            <td>{{ $itemtres->name }}</td>
            <td>{{ $itemtres->surname }}</td>
            <td>{{ $itemtres->phone }}</td>
            <td>{{ $item->email }}</td>

            <td>
              <div class="btn-group">
                @foreach ($doces as $do)
                @foreach ($especialidad as $esp)
                  @if ($do->doctor_id == $itemdos->id && $esp->id == $do->specialtie_id )
                      {{$esp->name}}<br> 
                  @endif
                @endforeach
                    
                @endforeach
                <br>
              </div>
            </td>
            <td>
              <div class="btn-group">
                @foreach ($hour as $ho)
                @foreach ($dochour as $doho)
                  @if ($doho->hour_id == $ho->id && $itemdos->id == $doho->doctor_id )
                     {{$ho->entrytime}}-{{$ho->departuretime}}<br>
                  @endif
                @endforeach
                    
                @endforeach
                <br>
              </div>
            </td>


            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Horario</button>

                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Desplegar menú</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li> <a type="button" class="btn btn-outline-secondary" data-dismiss="modal" href="{{route('agreghorario',$itemdos->external_id)}}" >
                    Agregar Horario
                  </a>
                  </li>
                  <li> <a type="button" class="btn btn-outline-secondary" data-dismiss="modal" href="{{route('modhora',$itemdos->external_id)}}" >
                    Modificar Horario
                  </a></li>
                  <li>
                    <a type="button" class="btn btn-outline-secondary" data-dismiss="modal" href="{{route('elimiarhorario',$itemdos->external_id)}}" >
                      Eliminar Horario
                    </a>
                  </li>


                </ul>
              </div>
             
          
              
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Especialidades</button>

                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Desplegar menú</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a type="button" class="btn btn-outline-secondary" data-dismiss="modal" href="{{route('agrespecilidad',$itemdos->external_id)}}" >
                      Agregar Especialidad
                    </a>
                  </li>
                  <li><a type="button" href="{{route('elimiarespe',$itemdos->external_id)}}" class="btn btn-danger">
                      Eliminar Especialidad
                    </a></li>
                  


                </ul>
              </div>

              </ul>
    </div>
    <a type="button" class="btn btn-outline-secondary" data-dismiss="modal" href="{{route('createmedico',$itemtres->external_id)}}" >
      Modificar
    </a>
    </td>
    </tr>
    @endif
    
    @endforeach
    @endforeach
    @endforeach
    @endforeach

    </tbody>
   
    </table>
  </div>
  </div>
</div>
@endif


<!--modaluno-->
@if ($verificar == true)

@else

@endif
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Médico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="for_doc" method="POST" action="{{route('createmediconuevo')}}">
          @csrf

          <div style="padding-left:20px; padding-right: 20px">
            <ul class="nav nav-tabs">
            </ul>
            <div class="tab-content" />
            <div role="tabpanel" class="tab-pane active" id="home">
              <div class="form-group">
                <label for="cedula">Cédula</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="cedula" name="cedula" placeholder="Ingrese su cédula">
                </div>
              </div>
              <div class="form-group">
                <label for="nombres">Nombres:</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese sus nombres">
                </div>
              </div>
              <div class="form-group">
                <label for="apellidos">Apellidos:</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="apellidos" name="apellidos"
                    placeholder="Ingrese sus apellidos">
                </div>
              </div>
              <div class="form-group">
                <label for="direccion">Dirección:</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="direccion" name="direccion"
                    placeholder="Ingrese su dirección">
                </div>
              </div>
              <div class="form-group">
                <label for="telefono">Teléfono:</label>
                <div class="input-group">
                  <input type="number" class="form-control " id="telefono" name="telefono"
                    placeholder="Ingrese su teléfono">
                </div>
              </div>
              <div class="form-group">
                <label for="genero">Seleccione su género:</label>
                <select name="genero" id="genero" class="form-control" aria-describedby="espeHelp">
                  <option value="masculino">Masculino</option>
                  <option value="femenino">Femenino</option>
                </select>
              </div>
              <div class="form-group">
                <label for="Fecha">Fecha de nacimiento:</label>
                <div class="input-group">
                  <input type="Date" class="form-control" id="fechas" name="fechas" placeholder="fecha">
                </div>
              </div>

            </div>
            <div class="form-group">
              <label for="usuario">Usuario:</label>
              <div class="input-group">
                <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Ingrese su usuario"
                  readonly>
              </div>
            </div>

            <div class="form-group">
              <label for="email">email:</label>
              <div class="input-group">
                <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese su email">
              </div>

              <div class="form-group">
                <label for="password">password:</label>
                <div class="input-group">
                  <input type="password" class="form-control" id="password" name="password"
                    placeholder="Ingrese su password">
                </div>
              </div>
              <div class="form-group" hidden>
                <select name="acceso" id="acceso" class="form-control" aria-describedby="espeHelp">
                  <option value="medico">medico</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="genero">Seleccione la especialidad:</label>

              <select name="especialidad" id="especialidad" class="form-control" aria-describedby="espeHelp">
                @foreach ($especialidad as $espe)
                <option value="{{$espe->external_id}}">{{$espe->name}}</option>
                @endforeach
              </select>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-outline-primary">Registrar</button>
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </form>
        <select hidden name="cedulas" id="cedulas">
          @foreach ($persona as $person)

          <option value="{{$person->cedula}}">{{$person->cedula}}</option>
          @endforeach
          <input hidden type="text" name="long" id="long" value="{{$tamanio}}">
        </select>

        <select hidden name="correos" id="correos">
          @foreach ($correo as $corr)
          <option value="{{$corr->email}}">{{$corr->email}}</option>
          @endforeach
          <input hidden type="text" name="tamaniocorreos" id="tamaniocorreos" value="{{$tamaniocorreo}}">
        </select>
      </div>

    </div>
  </div>
</div>






@endsection



<!-- Fin Navbar -->
@endsection

@section('scripts')

<script language="javascript" type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script>
  function recorrerSelect(sel) {
    var array = [sel.length];
    for (var i = 0; i < sel.length; i++) {
      var opt = sel[i];
      array[i] = opt.value;
    }
    return array;
  }

  $(document).ready(function () {
    $historiClinica = Math.floor((Math.random() * (10000 - 10)) + 10);
    $("#usuario").val("Doc-" + $historiClinica);


    var cedu = recorrerSelect(document.getElementById("cedulas"));
    var longitud = $('#long').val();
    $('#cedula').change(function () {

      var ced = $('#cedula').val();
      for (i = 0; i <= longitud; i++) {
        if (cedu[i] == ced) {
          Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: 'Esta identificación ya se encuentra registrada '
                });
          $('#cedula').val("");

        }
      }

    });

    var cor = recorrerSelect(document.getElementById("correos"));
    var longcor = $('#tamaniocorreos').val();

    $('#email').change(function () {
      var correoss = $('#email').val();
      for (i = 0; i <= longcor; i++) {
        if (cor[i] == correoss) {
          Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: 'El correo ya esta registrado, porfavor ingrese otro '
                });
          $('#email').val("");
        }
      }
    });



  });
</script>
<script language="javascript" type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript"
  src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script src="{{asset('assets/metodos.js')}}"></script>
<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection