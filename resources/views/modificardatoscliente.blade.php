



@extends('layouts.app')

@section('navegadorapp')
<a href="{{asset('/')}}" style=" margin-left:  90%;margin-block: 1%;" type="button" class="btn btn-outline-warning" >Cancelar</a>

@endsection
@section('navegador')




@section('cuerpo')
<div class="modal-body" style="margin-left: 25%;margin-right: 25%">
<form  method="post" action="{{route('modificardatoscliente', $persona)}}"> 
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    @csrf
    @method('put')
    
    <div style="padding-left:20px; padding-right: 20px">
        <ul class="nav nav-tabs">
        </ul>
        <div class="tab-content" />
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="form-group">
                <label for="cedula">Cédula</label>
                <div class="input-group">
                    <input type="number" disabled class="form-control" id="cedula" value="{{$persona->cedula}}" name="cedula" >
                </div>
            </div>
            <div class="form-group">
                <label for="nombres">Nombres:</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="nombres" name="nombres" value="{{$persona->name}}">
                </div>
            </div>
            <div class="form-group">
                <label for="apellidos">Apellidos:</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="apellidos" name="apellidos" value="{{$persona->surname}}" >
                </div>
            </div>
            <div class="form-group">
                <label for="direccion">Direccion:</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="direccion" name="direccion" value="{{$persona->cityResidence}}">
                </div>
            </div>
            <div class="form-group">
                <label for="telefono">Teléfono:</label>
                <div class="input-group">
                    <input type="number" class="form-control " id="telefono" name="telefono" value="{{$persona->phone}}">
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-primary">Modificar</button>
                <a href="{{asset('/')}}" type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</a>
            </div>
        </div>
        
            
    </div>
  </form>
</div>
@endsection
@endsection

