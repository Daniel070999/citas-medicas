@extends('layouts.app')

@section('navegadorapp')

@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
    <h2><a href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
    <ul class="nav navbar-nav">

      <li> <a href="{{route('homemedico')}}">
          Citas</a></li>
      <li> <a href="{{route('listarClientes')}}">
          Pacientes</a></li>

      <li><a href="{{route('salirdoc')}}">Cerrar sesión</a></li>
    </ul>
  </div>


</div>
<div class="w-100">
  <style>

  </style>


  <!-- Navbar -->

  @section('cuerpo')
  @if ($datos == false)
  <a type="text" style="font-size: 35px; margin-left: 15%">No hay examenes y historial clinico</a>
  @else
  <section class="u-align-center u-clearfix u-image u-section-1" id="carousel_9a61" data-image-width="1834"
    data-image-height="1080">
    <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
      <h1 class="u-custom-font u-font-raleway u-text u-text-palette-4-base u-text-1">
        <p class="u-text u-text-2">Historial Clinico!</p>
      </h1>
      <p class="u-large-text u-text u-text-variant u-text-2">Presentación de la información clínica del paciente </p>
      <p class="u-text u-text-3">Hospital<a href=""
          class="u-active-none u-border-1 u-border-black u-btn u-button-link u-button-style u-hover-none u-none u-text-body-color u-btn-1"
          target="_blank"></a>
      </p>
      <div class="u-clearfix u-expanded-width u-layout-wrap u-layout-wrap-1">
        <div class="u-layout">
          <div class="u-layout-row">
            <div class="u-align-left-sm u-align-left-xs u-container-style u-layout-cell u-size-18 u-layout-cell-1">
              <div class="u-container-layout u-valign-middle u-container-layout-1">
                <div alt="" class="u-border-9 u-border-white u-image u-image-circle u-image-1" data-image-width="700"
                  data-image-height="700"></div>
              </div>
            </div>
            <div class="u-container-style u-layout-cell u-size-24 u-layout-cell-2">
              <div class="u-container-layout u-container-layout-2">
                <h4 class="u-text u-text-4">Enfermedades Hereditarias</h4>
                @foreach ($historialmedico as $ite)
                <p class="u-text u-text-5"> {{$ite->diseasesHere}}.&nbsp;</p>
                @endforeach
                <h4 class="u-text u-text-4">Enfermedades </h4>
                @foreach ($historialmedico as $ite)
                <p class="u-text u-text-5"> {{$ite->Diseases}}.&nbsp;</p>
                @endforeach
                <h4 class="u-text u-text-4">Hábitos </h4>
                @foreach ($historialmedico as $ite)
                <p class="u-text u-text-5"> {{$ite->Diseases}}.&nbsp;</p>
                @endforeach


              </div>
            </div>
            <div class="u-container-style u-layout-cell u-size-18 u-layout-cell-3">
              <div class="u-container-layout u-container-layout-3">
                <h4 class="u-text u-text-7">Detalles del paciente</h4>
                <p class="u-text u-text-8">
                  <span style="font-weight: 700;">Cliente del paciente: </span>
                  <br>{{$persona->name}} {{$persona->surname}}<br>
                  <span style="font-weight: 700;">Nombre del paciente: </span>
                  <br>{{$client->name}}<br>
                  <span style="font-weight: 700;">Apellido del paciente: </span>
                  <br>{{$client->surname}} <span style="font-weight: 700;">
                    <br>Direccion del paciente:
                  </span>
                  <br>{{$client->cityResidence}}
                </p>

              </div>
              
            </div>
            <div class="container" style="padding-block: 5%">
              <h2>Diagnósticos
              </h2>
              <div class="row">
                <div class="col">
                  <table class="table table-striped table-bordered table-hover" id="tablas">
                    <thead>
                      <tr>
                        <th>Fecha de la Cita</th>
                        <th>Hora de la cita</th>
                        <th>Hora</th>
                        <th>Fecha</th>
                        <th>Tipo de exámen</th>
                        <th>Descripción </th>
                        <th>Diagnóstico</th>
                        <th>Resultado</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($mediExa as $diagnostic)
                      @foreach ($cita as $item)
                      @if ($diagnostic->quote_id == $item->id)
                      <tr>
                        <td>{{$item->datequotes}}</td>
                        <td>{{$item->hour}}</td>
                        <td>{{$diagnostic->hour}}</td>
                        <td>{{$diagnostic->examdate}}</td>
                        <td>{{$diagnostic->typeExa}}</td>
                        <td>{{$diagnostic->description}}</td>
                        <td>{{$diagnostic->diagnosis}}</td>
                        <td>{{$diagnostic->outcome}}</td>
                        <td>
                          <a href="{{route('rece',[$client->external_id,$item->external_id ])}}"
                            class="btn btn-primary ">Receta</a>
                          <a href="{{route('historialexamen',$diagnostic->external_id)}}" class="btn btn-primary">Agregar
                            Resultado</a>
                        </td>
          
                      </tr>
                      @endif
                      @endforeach
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
          
            </div>
          </div>
          
        </div>
  </section>

  @endif
  @endsection
  @endsection


@section('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<script>

  $(document).ready(function () {
    $('#tablas').DataTable({
      "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst": "Primero",
          "sLast": "Último",
          "sNext": "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }
    });

  });

</script>
@endsection