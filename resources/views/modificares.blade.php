@extends('layouts.plantilla')
@section('title', 'Cliente modificar'.$persona->nombre)
@section('navegador')
<nav class="navbar navbar-default navbar-fixed-top"> 
    <div class="container">
      <div class="col-md-12">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
          <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="{{route('listarClientes')}}">Regresar</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
 
@endsection

@section('fondo')
    <form  method="POST" action="{{route('update',$persona)}}"> 
    
    @csrf
    @method('PUT')
    <div style="padding-left:200px; padding-right: 200px">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">USUARIO</a></li>
        </ul>
        <div class="tab-content" />
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="form-group">
                <label for="cedula">Cedula</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="far fa-id-card"></i></span>
                    <input type="text" class="form-control" id="cedula" name="cedula" value="{{$persona->cedula}}">
                </div>
            </div>
            <div class="form-group">
                <label for="nombres">Nombres:</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="fas fa-signature"></i></span>
                    <input type="text" class="form-control" id="nombres" name="nombres" value="{{$persona->nombre}}">
                </div>
            </div>
            <div class="form-group">
                <label for="apellidos">Apellidos:</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="fab fa-adn"></i></span>
                    <input type="text" class="form-control" id="apellidos" name="apellidos" value="{{$persona->apellido}}">
                </div>
            </div>
            <div class="form-group">
                <label for="direccion">Direccion:</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                    <input type="text" class="form-control" id="direccion" name="direccion" value="{{$persona->Ciudadresidencia}}">
                </div>
            </div>
            <div class="form-group">
                <label for="telefono">Telefono:</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="fas fa-phone-volume"></i></span>
                    <input type="text" class="form-control " id="telefono" name="telefono" value="{{$persona->Telefono}}">
                </div>
            </div>
            <div class="form-group">
                <label for="genero">Genero:</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="fas fa-phone-volume"></i></span>
                    <input type="text" class="form-control " id="genero" name="genero" value="{{$persona->genero}}">
                </div>
            </div>
            <div class="form-group">
                <label for="Fecha">Fecha:</label>
                <div class="input-group">
                    <span class="input-group-text"><i class="fas fa-phone-volume"></i></span>
                    <input type="date" class="form-control " id="fecha" name="fecha" value="{{$persona->FechaNacimiento}}">
                </div>
            </div>
        </div>
        <button type="submit">Actualizar datos</button>
    </div>
    </form> 
@endsection


@section('cuerpo')



@endsection
