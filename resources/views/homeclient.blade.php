 
@extends('layouts.app')

@section('navegadorapp')
@endsection 

@section('navegador') 
<div class="col-md-3 col-sm-3 col-xs-12">
    <div class="logo">
        <h2><a href="#"></a></h2>
    </div>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="menu">
        <ul class="nav navbar-nav">
            <li class="active"><a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
            <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
            <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
            <li><a href="{{route('listaradmin')}}">Clientes</a></li>
            <li> <a  href="{{route('especialida')}}">Especialidad</a>
          </li>
            <li><a  href="{{route('salirAdmin')}}">Cerrar sesión</a></li>
        </ul>
    </div>
  </div>
    <!-- Start header -->
    
  
  <!-- Header End -->
@endsection
@section('cuerpo')
  <!--modal-->
  <div class="modal fade" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Registro de Pacientes</h4>
            </div>
            <div class="modal-body">
                <form id="formulario" method="POST" action="{{route('registrarclien')}}">
                    
                        @csrf
                        <div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos cita</a></li>
                            </ul>
    
                            <!-- Tab panes --> 
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="form-group">
                                        <label for="cedula">Cédula</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Ingrese su cédula">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nombres">Nombres:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese sus nombres">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="apellidos">Apellidos:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Ingrese sus apellidos">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion">Dirección:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese su dirección">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telefono">Teléfono:</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control " id="telefono" name="telefono" placeholder="Ingrese su teléfono">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telefono">Género:</label>
                                        <select name="genero" id="genero" class="form-control" aria-describedby="espeHelp">
                                            <option value="masculino">Masculino</option>
                                            <option value="femenino">Femenino</option>
                                        </select>            
                                    </div>
                                    <div class="form-group">
                                        <label for="fecha">Fecha de nacimiento:</label>
                                        <div class="input-group">
                                            <input type="Date" class="form-control " id="fecha" name="fecha" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="historia">Historia Clínica:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="historia" name="historia" readonly>
                                        </div>
                                    </div>
                                    <label for="email">email:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese su email">
                                    </div>
                    
                                    <div class="form-group">
                                    <label for="password">Clave:</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Ingrese su password">
                                    </div>
                                </div>
                                    <label name="acceso" id="acceso" hidden>
                                        <input type="text" value="cliente" id="acceso" name="acceso">
                                    </label>            
                                </div>
                                
    
                            </div>
                        </div>
                        <div class="modal-footer">                                    
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                </form>
                <select hidden name="cedulas" id="cedulas">
                    @foreach ($personas as $person)
                        
                        <option value="{{$person->cedula}}">{{$person->cedula}}</option>
                     @endforeach
                     <input hidden type="text" name="long" id="long" value="{{$tamanio}}">
                        </select>
    
                        <select hidden name="correos" id="correos">
                          @foreach ($correo as $corr)
                              <option value="{{$corr->email}}">{{$corr->email}}</option>
                           @endforeach
                           <input hidden type="text" name="tamaniocorreos" id="tamaniocorreos" value="{{$tamaniocorreo}}">
                      </select>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin modal -->


<div class="container" style="padding-block: 5%">
  <h2>Lista de Clientes
    <a href="" class="btn btn-primary"  data-toggle="modal" data-target="#modal" style="margin-left: 5%">Nuevo</a>
  </h2>
  <div class="panel">

    @if ($datos == false)
    <p class="h3 text-center">No hay Clientes registrados</p>
    @else
  <div class="row">
    <div class="col">
      <table class="table table-striped table-bordered table-hover" id="tablas">
        <thead>
          <tr>
            <th>Cedula</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>género</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody> 
            
            @foreach ($clientes as $item)
            @foreach ($personas as $ite)
            @if ( $item->people_id == $ite->id)
            <tr>
                <td>{{$ite->cedula}}</td>
                <td>{{$ite->name}}</td>
                <td>{{$ite->surname}}</td>
                <td>{{$ite->gender}}</td> 
                <td><a href="{{route('pasienteadmin',$item->external_id)}}" type="button" class="btn btn-outline-secondary" data-dismiss="modal">Crear paciente</a>
                <a href="{{route('listapacadmin',$item->external_id)}}" type="button" class="btn btn-outline-secondary" data-dismiss="modal">Pacientes</a></td>
            </tr> 
            @endif
            @endforeach                 
            @endforeach
        </tbody>
      </table>
    </div>
  </div>

  @endif
  
@endsection
@section('scripts')
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script src="{{asset('assets/metodos.js')}}"></script>
<script>
   
   $(document).ready(function () {
        $historiClinica = Math.floor((Math.random() * (10000 - 10)) + 10);
    $("#historia").val("HI-"+$historiClinica);
    
    var cedu = recorrerSelect(document.getElementById("cedulas"));
    var longitud = $('#long').val();
    
    $('#cedula').change(function(){
        var ced = $('#cedula').val();
        for (i = 0 ;i <= longitud; i++){
            if (cedu[i] == ced) {
                Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: 'Esta identificación ya se encuentra registrada '
                });
            $('#cedula').val("");
        }
        }
    });
    
    var cor = recorrerSelect(document.getElementById("correos"));
    var longcor = $('#tamaniocorreos').val();
    
    $('#email').change(function(){
        var correoss = $('#email').val();
        for (i = 0 ;i <= longcor; i++){
            if (cor[i] == correoss) {
                Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: 'El correo ya esta registrado, porfavor ingrese otro '
                });
            $('#email').val("");
        }
        }
    });
    
    
    });
</script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection