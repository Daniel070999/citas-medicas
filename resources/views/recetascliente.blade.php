

@extends('layouts.app')

@section('navegadorapp')
    
@endsection
@section('navegador')
<!-- Sidebar -->

<!-- Fin sidebar -->

<div class="w-100">
  <style>
    
    </style>
      
  
 <!-- Navbar -->
 
  @section('cuerpo')
  <a href="{{asset('/')}}" style=" margin-left:  90%;margin-block: 1%;" type="button" class="btn btn-outline-warning" >Cancelar</a>
  <div class="container" style="padding-block: 5%">
    <h2>Recetas del Paciente
    <!--<a href="{{asset('createmedico')}}" class="btn btn-primary" style="margin-left: 5%">Nuevo</a> -->
    </h2>
    
    
    <style>
        .contenedor {
          display: flex;
        }
      .flip-card {
        display: block;
        position: relative;
        z-index: 1000;
        width: 250px;
        height: 330px;
        }
       .flip-card .card-front,
       .flip-card .card-back {
       -moz-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
       -moz-transition: -moz-transform 500ms;
       -o-transition: -o-transform 500ms;
       -webkit-transition: -webkit-transform 500ms;
       transition: transform 500ms;
       display: block;
       height: 100%;
       position: absolute;
       width: 100%;
       }
      
       .flip-card .card-front {
       -moz-transform: perspective(300) rotateY(0);
       -webkit-transform: perspective(300) rotateY(0);
       transform: perspective(300) rotateY(0);
       z-index: 900;
        }
         .flip-card .card-back {
           -moz-transform: rotateY(-180deg);
           -webkit-transform: rotateY(-180deg);
           transform: rotateY(-180deg);
           z-index: 800;
         }
         .flip-card:hover .card-front {
           -moz-transform: rotateY(180deg);
           -ms-transform: rotateY(180deg);
           -webkit-transform: rotateY(180deg);
           transform: rotateY(180deg);
           -moz-transform: perspective(300) rotateY(180deg);
           -webkit-transform: perspective(300) rotateY(180deg);              
           transform: perspective(300) rotateY(180deg);
         }
         .flip-card:hover .card-back {
           z-index: 950;
           transform: rotateY(0deg);
           transform: perspective(300) rotateY(0deg);
         }
      
         /*** Just for show... ***/
         .flip-card {
           color: #000;
           cursor: pointer;
           float: left;
           font-weight: bold;
           margin: 10px;
           text-align: center;
           text-transform: uppercase;
           min-width: 100px;
           max-width: 400px;
         }
         .flip-card .card-front,
         .flip-card .card-back {
           border-radius: 10px;
           box-shadow: 1px 1px 2px rgba(14, 85, 143, 0.8);
           box-sizing: border-box;
           padding: 40px 0;
         }
         .flip-card .card-front {
           box-shadow: 0 0 20px rgba(115, 113, 113, 0.94) inset;
           box-sizing: border-box;
           background-color: #fff;
           *zoom: 1;
           filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#FF499BEA',                      
           endColorstr='#FF207CE5');
           background-size: 100%;
         }
         .flip-card .card-back {
           box-shadow: 0 0 20px rgba(115, 113, 113, 0.94) inset;
           background-color: #fff;
           *zoom: 1;
           filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#FF478CE0',        
          endColorstr='#FF0263DB');
           background-size: 100%;
         }
      
         body {
      
         }
      
         .viewport {
           margin: 10px auto 0;
           width: 500px;
         }
      
         .flip-card .card-front img {
           margin-top: 30px;
         }
      </style>
      <section class="contenedor">
        @foreach ($receta as $item)
        @if ($item->patient_id == $pasiente->id)
        <div class="viewport">
            <div class="flip-card">
             <div class="card-front">
             <p>Código:CA-{{$item->id}} 
             <br>
             </p>
             <p>Producto:<br>{{$item->product}} 
               
            </p>
             <p>Cantidad:{{$item->amount}} 
             </p> 
             <p>Descripción:{{$item->description}} 
             </p>
             <img class="residuo" src="{{asset('assets/droga.png')}}"  width="50" height="50">
             </div>
           <div class="card-back">
             <p>Receta </p>
             <img src="{{asset('assets/prescripcion-medica.png')}} " width="50" height="50">
              </div>
             </div>
           </div> 
        @endif    
      @endforeach
      </section>
  @endsection
@endsection


