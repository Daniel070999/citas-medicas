

@extends('layouts.app')

@section('navegadorapp') 
@endsection
@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#"></a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
          <li class="active"><a class="nav-link active" href="{{asset('/')}}">Doctores</a></li>
          <li><a href="{{asset('listarcitasadmin')}}">Citas</a></li>
          <li><a href="{{route('pacienteadmin')}}">Pacientes</a></li>
          <li><a href="{{route('listaradmin')}}">Clientes</a></li>
          <li> <a  href="{{route('especialida')}}">Especialidad</a>
        </li>
          <li><a  href="{{route('salirAdmin')}}">Cerrar sesión</a></li>
      </ul>
  </div>
</div>
  <!-- Start header -->
  

<!-- Header End -->
@section('cuerpo')

<!-- Button trigger modal -->





<div class="container" style="padding-block: 5%">
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <h2>Lista de Especialidades
    <a href="" class="btn btn-primary"  data-toggle="modal" data-target="#modal" style="margin-left: 5%">Nuevo</a>
  </h2>
  @if ($datos == false)
  <p class="h3 text-center">Aun no hay especialidades registradas</p>
  @else
      

    <div class="col">
      <table class="table table-striped table-bordered table-hover" id="tablas">
        <thead>
          <tr>
            <th>Nombres</th>
            <th>Estado</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>
            
                @foreach ($especialida as $item)
                        <tr>
                          <td>{{ $item->name }}</td>
                          @if ( $item->active== 1)
                              <td>Activo</td>
                              <td>
                                <form method="POST" action="{{route('desactivarespecialidad',$item->external_id)}}">
                                @csrf
                                  <button type="submit" class="btn btn-warning">
                                Desactivar
                              </button>
                            </form></td>
                          @else
                          <td>Desactivado</td>
                          <td>
                            <form method="POST" action="{{route('activarespecialidad',$item->external_id)}}">
                            @csrf
                              <button type="submit" class="btn btn-success">
                            Activar
                          </button>
                        </form></td>
                          @endif
                          
                        </tr>
              @endforeach       
        </tbody> 
      </table>
    </div>
  </div>
  @endif
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registrar Especialidad </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form  method="POST" action="{{route('especialidagu')}}"> 
                @csrf
                
                <div style="padding-left:20px; padding-right: 20px">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">Especialidad</a></li>
                    
                    </ul>
                    <div class="tab-content" />
                    <div role="tabpanel" class="tab-pane active" id="home">
                    
                        <div class="form-group">
                            <label for="nombres">Nombre de la especialidad:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese sus nombres">
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-primary">Registrar</button>
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                
              </form>
        </div>
        
      </div>
    </div>
  </div>
  

    

    

@endsection



  <!-- Fin Navbar -->
@endsection
@section('scripts')
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script src="{{asset('assets/funciones.js')}}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
<script src="{{asset('assets/metodos.js')}}"></script>
<script>
  
   $(document).ready(function () {
    $('#tablas').DataTable({
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});    
     
});

</script>
@endsection