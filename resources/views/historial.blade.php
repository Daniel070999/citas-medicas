@extends('layouts.app')

@section('navegadorapp')
@endsection 

@section('navegador')
<div class="col-md-3 col-sm-3 col-xs-12">
    <div class="logo">
        <h2><a href="#">Kulo</a></h2>
    </div>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="menu">
        <ul class="nav navbar-nav">
          <li> <a href="{{route('homemedico')}}">
            Incio</a></li>
            <li> <a href="{{route('homemedico')}}">
              Citas</a></li>
          <li> <a href="{{route('listarClientes')}}"  >
            Pacientes</a></li>
          
          
        </ul>
    
         
    </div>
  </div>
@endsection
@section('cuerpo')
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            
            <div class="card-body">
                <form  id="formulario" method="POST" action="{{route('agregarhistorial',$persona->external_id)}}"> 
                    @csrf
                    @method('put')
                    
                    <div style="padding-left:150px; padding-right: 150px">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">DATOS GENERALES</a></li>
                           
                        </ul>
                        <div class="tab-content" />
                        <div role="tabpanel" class="tab-pane active" id="home">
                           <!-- Tab panes -->

                      <div class="form-group">
                        <label for="enf" class="control-label">Enfermedades:</label>
                        <input type="text" class="form-control" id="enf" name="enf">
                    </div>
                    <div class="form-group">
                        <label for="enf_her" class="control-label">Enfermedades hederitarias:</label>
                        <input type="text" class="form-control" id="enf_her" name="enf_her">
                    </div>
                    <div class="form-group">
                        <label for="hab" class="control-label">Habitos:</label>
                        <input type="text" class="form-control" id="hab" name="hab">
                    </div>
                    
                </div>
                <h2>Información para pedido de exámen</h2>
                <div class="form-group">
                    <label for="enf" class="control-label">Diagnóstico:</label>
                    <input type="text" class="form-control" id="enf" name="enf">
                </div>
                <label class="text-black">Tipo de  examen</label>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                      <div class="form-group">
                        <select name="tipoExa" id="tipoExa" class="form-control" aria-describedby="espeHelp">
                            <option value="Hemograma completo">Hemograma completo</option>
                            <option value="Urinálisis completo">Urinálisis completo</option>
                            <option value="Heces por parásito, sangre oculta">Heces por parásito, sangre oculta</option>
                            <option value="Perfil lipídico: Colesterol, LDL; HDL; triglicérido">Perfil lipídico: Colesterol, LDL; HDL; triglicérido</option>
                        </select>            
                        <small id="espeHelp" class="form-text text-muted">seleccione el tipo de examen.</small>
                    </div>
                    <div class="form-group">
                        <label for="enf" class="control-label">Seleccione Fecha:</label>
                      <input type="date" id="fechaex" min="<?php $hoy=date("Y-m-d"); echo $hoy;?>" name="fechaex" class="form-control">
                    </div>
                    <div class="form-group">
                      
                        <label for="enf" class="control-label">Seleccione hora:</label>
                      <select multiple class="form-control text-black dropdown-secondary" name="hora" id="horario" size="8">
                          <option value="8:00-8:30" id="h1">8:00-8:30</option>
                          <option value="8:30-9:00" id="h2">8:30-9:00</option>
                          <option value="9:00-9:30" id="h3">9:00-9:30</option>
                          <option value="9:30-10:00" id="h4">9:30-10:00</option> 
                          <option value="10:00-10:30" id="h5">10:00-10:30</option>
                          <option value="10:30-11:00" id="h6">10:30-11:00</option>
                          <option value="11:00-11:30" id="h7">11:00-11:30</option>
                          <option value="11:30-12:00" id="h8">11:30-12:00</option>
                          <option value="12:00-12:30" id="h9">12:00-12:30</option>
                          <option value="almuerzo" disabled=""  >Hora de almuerzo</option>
                          <option value="16:00-16:30" id="h12">16:00-16:30</option>
                          <option value="16:30-17:00" id="h13">16:30-17:00</option>
                          <option value="16:00-16:30" id="h14">17:00-17:30</option>
                          <option value="16:30-17:00" id="h15">17:30-18:00</option>
                          <option value="16:00-16:30" id="h16">18:00-18:30</option>
                      </select>
                  </div>
                        <div class="form-group">
                          <label for="descripcion" class="control-label">Descripción:</label>
                          <div class="modal-body">
                            <textarea class="form-control col-xs-12" id="descripcion" name="descripcion"></textarea>
                        </div>

                
                <div role="tabpanel" class="tab-pane active" id="home">
                    <label class="text-black">Seleccione Producto</label>
                    <select class="selectpicker form-control" multiple id="selectlistauno" name="selectlistauno[]">
                    <option value="Aspirina">Aspirina</option>
                    <option value="Omeprazol">Omeprazol</option>
                    <option value="Lexotiroxina sodica">Lexotiroxina sódica</option>
                    <option value="Ramipril">Ramipril</option>
                    <option value="Paracetamol">Paracetamol </option>
                    <option value="Atorvastatina ">Atorvastatina </option>
                    <option value="Salbutamol">Salbutamol </option>
                      </select>
                    
                    <div class="form-group">
                        <label for="cantidad">Cantidad:</label>
                        <div class="input-group">
                            <input type="number" class="form-control " id="cantidad" name="cantidad" placeholder="Ingrese cantidad">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion" class="control-label">Descripción:</label>
                        <div class="modal-body">
                          <textarea class="form-control col-xs-12" id="descripcion" name="descripcion"></textarea>
                      </div>    
                        
                        
                </form>
               
               
            </div>
            <input type="text" hidden value="{{$cita->external_id}}" id="cita" name="cita">
            <div class="modal-footer">                                    
                <a type="button" class="btn btn-default" data-dismiss="modal" href="{{route('homemedico')}}">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
@endsection