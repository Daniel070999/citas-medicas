@extends('layouts.app')

@section('navegadorapp')

@endsection
@section('navegador')

<div class="col-md-3 col-sm-3 col-xs-12">
  <div class="logo">
      <h2><a href="#">Kulo</a></h2>
  </div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="menu">
      <ul class="nav navbar-nav">
        
          <li><a href="{{asset('homecliente')}}" >
            Inicio</a></li>
        <li> <a href="{{route('citapa', Auth::user()->external_id)}}"  >
            Citas</a></li>
        <li> <a href="{{route('paciente',Auth::user()->external_id)}}">
            Pacientes</a></li>
        <li><a class="dropdown-item" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
          Cerrar sesión
       </a></li>
      <li> <a class="dropdown-item" href="{{route('modcliente',Auth::user()->external_id) }}">
        Modificar datos
     </a></li>
      </ul>
  </div>
       
  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</div>


  
 <!-- Navbar -->

  <!-- Fin Navbar -->
@endsection

@section('cuerpo')
    <a type="text" style="font-size: 35px; margin-left: 15%">Bienvenido a su sistema de citas, su Historia clínica es: {{Auth::user()->name}}</a>
@endsection
