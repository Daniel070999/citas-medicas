<!doctype html>

<html lang="en">

<head>

  <style>
    body {
      font-family: sans-serif;
    }

    @page {
      margin: 160px 50px;
    }

    header {
      position: fixed;
      left: 0px;
      top: -160px;
      right: 0px;
      height: 100px;
      background-color: #ddd;
      text-align: center;
    }

    header h1 {
      margin: 10px 0;
    }
    H2.center{
   text-align: center;
    }
    header h2 {
      margin: 0 0 10px 0;
    }

    footer {
      position: fixed;
      left: 0px;
      bottom: -50px;
      right: 0px;
      height: 40px;
      border-bottom: 2px solid #ddd;
    }

    footer .page:after {
      content: counter(page);
    }

    footer table {
      width: 100%;
    }

    footer p {
      text-align: right;
    }

    footer .izq {
      text-align: left;
    }

    table {
   width: 100%;
   border: 1px solid #000;
}
th, td {
   width: 25%;
   text-align: left;
   vertical-align: top;
   border: 1px solid #000;
   border-collapse: collapse;
   padding: 0.3em;
   caption-side: bottom;
}
caption {
   padding: 0.3em;
   color: #fff;
    background: #000;
}
th {
   background: #eee;
}
  </style>

<body>
  <header>
    <h1>Hospital Nuestra Familia</h1>
    <h2>Recetas</h2>
  </header>
  <div class="container" style="padding-block: 5%">
    <h2 >Recetas
    </h2>
    <div class="row">
      <div class="col">
        
        <div class="container">
          <h4 class="u-text u-text-4">Enfermedades Hereditarias</h4>
          @foreach ($enfermedad as $ite)
          <p class="u-text u-text-5"> {{$ite->diseasesHere}}.&nbsp;</p>
          @endforeach
          <h4 class="u-text u-text-4">Enfermedades </h4>
          @foreach ($enfermedad as $ite)
          <p class="u-text u-text-5"> {{$ite->Diseases}}.&nbsp;</p>
          @endforeach
          <h4 class="u-text u-text-4">Hábitos </h4>
          @foreach ($enfermedad as $ite)
          <p class="u-text u-text-5"> {{$ite->Diseases}}.&nbsp;</p>
          @endforeach


        </div>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>Paciente</th>
              <th>Producto</th>
              <th>Cantidad</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($mediExa as $diagnostic)
            <tr>
              <td>{{$persona->name}} {{$persona->surname}}</td>
              <td>{{$diagnostic->product}}</td>
              <td>{{$diagnostic->amount}}</td>
              <td>{{$diagnostic->description}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>

</html>