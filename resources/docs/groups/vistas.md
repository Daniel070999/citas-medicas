# Vistas

APIs para manejo de errores, ventanas principales

## Para cargar los datos para ingresar una nueva cita




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/nuevacitaadmin/5" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/nuevacitaadmin/5"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
 "roles": ["administrador"]
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnuevacitaadmin--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnuevacitaadmin--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnuevacitaadmin--id-"></code></pre>
</div>
<div id="execution-error-GETnuevacitaadmin--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnuevacitaadmin--id-"></code></pre>
</div>
<form id="form-GETnuevacitaadmin--id-" data-method="GET" data-path="nuevacitaadmin/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnuevacitaadmin--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnuevacitaadmin--id-" onclick="tryItOut('GETnuevacitaadmin--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnuevacitaadmin--id-" onclick="cancelTryOut('GETnuevacitaadmin--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnuevacitaadmin--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nuevacitaadmin/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="id" data-endpoint="GETnuevacitaadmin--id-" data-component="url" required  hidden>
<br>
El external id de la persona</p>
</form>


## Para cargar los doctores




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/createmedico/ex" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/createmedico/ex"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
 "roles": ["administrador"]
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETcreatemedico--external-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETcreatemedico--external-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETcreatemedico--external-"></code></pre>
</div>
<div id="execution-error-GETcreatemedico--external-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETcreatemedico--external-"></code></pre>
</div>
<form id="form-GETcreatemedico--external-" data-method="GET" data-path="createmedico/{external}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETcreatemedico--external-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETcreatemedico--external-" onclick="tryItOut('GETcreatemedico--external-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETcreatemedico--external-" onclick="cancelTryOut('GETcreatemedico--external-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETcreatemedico--external-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>createmedico/{external}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>external</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="external" data-endpoint="GETcreatemedico--external-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="id" data-endpoint="GETcreatemedico--external-" data-component="url" required  hidden>
<br>
El external id de la persona</p>
</form>


## Para redireccionar la ventana principal




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito"
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GET-" hidden>
    <blockquote>Received response<span id="execution-response-status-GET-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GET-"></code></pre>
</div>
<div id="execution-error-GET-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GET-"></code></pre>
</div>
<form id="form-GET-" data-method="GET" data-path="/" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GET-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GET-" onclick="tryItOut('GET-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GET-" onclick="cancelTryOut('GET-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GET-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>/</code></b>
</p>
</form>


## Para redireccionar al cancelar una cita




> Example request:

```bash
curl -X POST \
    "http://CitasMedicas.test/cancelarcita/nam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/cancelarcita/nam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
 "roles": ["administrador"]
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
<div id="execution-results-POSTcancelarcita--externalID-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTcancelarcita--externalID-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTcancelarcita--externalID-"></code></pre>
</div>
<div id="execution-error-POSTcancelarcita--externalID-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTcancelarcita--externalID-"></code></pre>
</div>
<form id="form-POSTcancelarcita--externalID-" data-method="POST" data-path="cancelarcita/{externalID}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTcancelarcita--externalID-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTcancelarcita--externalID-" onclick="tryItOut('POSTcancelarcita--externalID-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTcancelarcita--externalID-" onclick="cancelTryOut('POSTcancelarcita--externalID-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTcancelarcita--externalID-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>cancelarcita/{externalID}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>externalID</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="externalID" data-endpoint="POSTcancelarcita--externalID-" data-component="url" required  hidden>
<br>
</p>
</form>


## Para redireccionar la ventana principal  del cliente




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/homecliente" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/homecliente"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GEThomecliente" hidden>
    <blockquote>Received response<span id="execution-response-status-GEThomecliente"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GEThomecliente"></code></pre>
</div>
<div id="execution-error-GEThomecliente" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GEThomecliente"></code></pre>
</div>
<form id="form-GEThomecliente" data-method="GET" data-path="homecliente" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GEThomecliente', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GEThomecliente" onclick="tryItOut('GEThomecliente');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GEThomecliente" onclick="cancelTryOut('GEThomecliente');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GEThomecliente" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>homecliente</code></b>
</p>
</form>


## Para redireccionar la ventana principal del doctor




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/homemedico" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/homemedico"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GEThomemedico" hidden>
    <blockquote>Received response<span id="execution-response-status-GEThomemedico"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GEThomemedico"></code></pre>
</div>
<div id="execution-error-GEThomemedico" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GEThomemedico"></code></pre>
</div>
<form id="form-GEThomemedico" data-method="GET" data-path="homemedico" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GEThomemedico', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GEThomemedico" onclick="tryItOut('GEThomemedico');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GEThomemedico" onclick="cancelTryOut('GEThomemedico');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GEThomemedico" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>homemedico</code></b>
</p>
</form>


## Para listar todas las citas donde el administrador




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/listarcitasadmin" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/listarcitasadmin"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
 "roles": ["administrador"]
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETlistarcitasadmin" hidden>
    <blockquote>Received response<span id="execution-response-status-GETlistarcitasadmin"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETlistarcitasadmin"></code></pre>
</div>
<div id="execution-error-GETlistarcitasadmin" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETlistarcitasadmin"></code></pre>
</div>
<form id="form-GETlistarcitasadmin" data-method="GET" data-path="listarcitasadmin" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETlistarcitasadmin', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETlistarcitasadmin" onclick="tryItOut('GETlistarcitasadmin');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETlistarcitasadmin" onclick="cancelTryOut('GETlistarcitasadmin');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETlistarcitasadmin" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>listarcitasadmin</code></b>
</p>
</form>


## Para cargar los datos para ingresar una nueva cita




> Example request:

```bash
curl -X PUT \
    "http://CitasMedicas.test/modificardatosmedicos/pariatur?nombres=eum&apellidos=veniam&Ciudadresidencia=reiciendis&genero=et&telefono=sed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/modificardatosmedicos/pariatur"
);

let params = {
    "nombres": "eum",
    "apellidos": "veniam",
    "Ciudadresidencia": "reiciendis",
    "genero": "et",
    "telefono": "sed",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
 "roles": ["administrador"]
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
<div id="execution-results-PUTmodificardatosmedicos--doctor-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTmodificardatosmedicos--doctor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTmodificardatosmedicos--doctor-"></code></pre>
</div>
<div id="execution-error-PUTmodificardatosmedicos--doctor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTmodificardatosmedicos--doctor-"></code></pre>
</div>
<form id="form-PUTmodificardatosmedicos--doctor-" data-method="PUT" data-path="modificardatosmedicos/{doctor}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTmodificardatosmedicos--doctor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTmodificardatosmedicos--doctor-" onclick="tryItOut('PUTmodificardatosmedicos--doctor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTmodificardatosmedicos--doctor-" onclick="cancelTryOut('PUTmodificardatosmedicos--doctor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTmodificardatosmedicos--doctor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>modificardatosmedicos/{doctor}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>doctor</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="doctor" data-endpoint="PUTmodificardatosmedicos--doctor-" data-component="url" required  hidden>
<br>
El external id de la persona</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>nombres</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="nombres" data-endpoint="PUTmodificardatosmedicos--doctor-" data-component="query" required  hidden>
<br>
</p>
<p>
<b><code>apellidos</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="apellidos" data-endpoint="PUTmodificardatosmedicos--doctor-" data-component="query" required  hidden>
<br>
</p>
<p>
<b><code>Ciudadresidencia</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="Ciudadresidencia" data-endpoint="PUTmodificardatosmedicos--doctor-" data-component="query" required  hidden>
<br>
</p>
<p>
<b><code>genero</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="genero" data-endpoint="PUTmodificardatosmedicos--doctor-" data-component="query" required  hidden>
<br>
</p>
<p>
<b><code>telefono</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="telefono" data-endpoint="PUTmodificardatosmedicos--doctor-" data-component="query" required  hidden>
<br>
</p>
</form>


## Para cerrar sesión




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/salirAdmin" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/salirAdmin"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito"
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETsalirAdmin" hidden>
    <blockquote>Received response<span id="execution-response-status-GETsalirAdmin"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETsalirAdmin"></code></pre>
</div>
<div id="execution-error-GETsalirAdmin" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETsalirAdmin"></code></pre>
</div>
<form id="form-GETsalirAdmin" data-method="GET" data-path="salirAdmin" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETsalirAdmin', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETsalirAdmin" onclick="tryItOut('GETsalirAdmin');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETsalirAdmin" onclick="cancelTryOut('GETsalirAdmin');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETsalirAdmin" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>salirAdmin</code></b>
</p>
</form>


## Para listar los pacientes al administrador




> Example request:

```bash
curl -X GET \
    -G "http://CitasMedicas.test/pacienteadmin" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://CitasMedicas.test/pacienteadmin"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500, success):

```json

{
 "name": "Proceso con exito",
 "roles": ["administrador"]
}
 @response scenario="user not found" {
"message": "User not found"
 }
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETpacienteadmin" hidden>
    <blockquote>Received response<span id="execution-response-status-GETpacienteadmin"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETpacienteadmin"></code></pre>
</div>
<div id="execution-error-GETpacienteadmin" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETpacienteadmin"></code></pre>
</div>
<form id="form-GETpacienteadmin" data-method="GET" data-path="pacienteadmin" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETpacienteadmin', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETpacienteadmin" onclick="tryItOut('GETpacienteadmin');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETpacienteadmin" onclick="cancelTryOut('GETpacienteadmin');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETpacienteadmin" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>pacienteadmin</code></b>
</p>
</form>



