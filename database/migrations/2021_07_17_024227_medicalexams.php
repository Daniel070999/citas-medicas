<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Medicalexams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicalexams', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->string('typeExa');
            $table->string('examdate');
            $table->string('hour');
            $table->string('diagnosis');
            $table->string('outcome'); 
            $table->foreignId('patient_id')->constrained()->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('quote_id')->constrained()->onUpdate('cascade')
                ->onDelete('cascade');
                
            $table->uuid('external_id');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicalexams');
    }
}
