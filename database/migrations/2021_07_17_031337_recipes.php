<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Recipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
	    $table->string('product');
        $table->integer('amount');
        $table->string('description');
            $table->foreignId('patient_id')->constrained()->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('quote_id')->constrained()->onUpdate('cascade')
            ->onDelete('cascade');
            $table->uuid('external_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
