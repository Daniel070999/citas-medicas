<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Quotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
	    $table->date('datequotes');
	    $table->integer('state');
        $table->text('quotetype');
	    $table->text('hour', $precision = 0);
        $table->foreignId('doctor_id')->constrained()->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('patient_id')->constrained()->onUpdate('cascade')
            ->onDelete('cascade');
        $table->foreignId('specialtie_id')->constrained()->onUpdate('cascade')
            ->onDelete('cascade');    
        $table->uuid('external_id');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
