<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TodosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $useradmin=User::create([
            'name'=>'administradorlaravel',
            'email'=>'encargago.laravel@gmail.com',
            'password'=>Hash::make('admin@12345'),
            'acceso'=>'admin',
            'external_id'=>Str::uuid()->toString(),
        ]);
        
    }
}
